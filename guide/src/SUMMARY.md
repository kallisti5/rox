# Summary

- [Rox](README.md)
- [Introduction](intro/introduction.md)
    - [Installation](intro/installation.md)
	- [Inverting Some Bits](intro/inverting.md)
- [The Basics](basics/basics.md)
    - [Types](basics/types.md)
    - [Assignment](basics/assignment.md)
	- [Operators](basics/operators.md)
    - [Constants](basics/constants.md)
    - [Comments](basics/comments.md)
    - [Conditional Statements](basics/conditionals.md)
-----------
[Contributors](misc/contributors.md)
