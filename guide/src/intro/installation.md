# Installation

Rox is still in early development, and currently there are no
distributed binaries. To use Rox, you will need to build it from
source. Rox is developed in [Rust], and to install Rox you need to
install the Rust compiler `rustc` as well as the Rust build tool
`cargo`. Instructions to install these can be found on the
[Rust] page.

Rox makes use of multi-precision libraries that will need to be built
while building Rox. This should be automatic on GNU/Linux and macOS,
but might require some care on Windows. For details on building on
Windows, see the `gmp-mpfr-sys` [documentation][gmp-mpfr-sys doc].

Once that is out of the way, there are different ways to install
Rox. The easiest is the first below.

1. Install the latest Rox release from [crates.io]. In the terminal
   execute:

   ```sh
   cargo install rox
   ```

   This will download the latest Rox release and install it in the
   cargo path. If Rust is installed properly, you can now run the Rox
   compiler. Try:

   ```sh
   rox --help
   ```

2. Install from the [git repository] to get the version under current
   development. In the terminal execute:

   ```sh
   cargo install --git https://gitlab.com/tspiteri/rox.git
   ```

3. If you want to view or modify the source code of Rox, the easiest
   way is to clone the [git repository] and build it on your machine.
   
   ```sh
   git clone https://gitlab.com/tspiteri/rox.git
   cd rox
   cargo build
   ```
   
   The executable file `rox` should be built in the directory
   *rox/target/debug*. If you want to build the optimized version
   rather than the debug version, use `cargo build --release`, and the
   optimized executable will be built in the directory
   *rox/target/release*.

[Rust]: https://www.rust-lang.org
[crates.io]: https://crates.io
[git repository]: https://gitlab.com/tspiteri/rox
[gmp-mpfr-sys doc]: https://docs.rs/gmp-mpfr-sys/*/gmp_mpfr_sys/index.html#building-on-windows
