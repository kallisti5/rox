# Inverting Some Bits

Once Rox is installed, you can write a simple Rox program. As an
example, let us write a simple component that inverts its input.

At this stage, libray and project support is not yet developed, so
`rox` will work on source files directly. Create a file called
*invert.rox* in your editor and type the following:

```rox
fn invert(in: unsigned(6)) -> (inverted: unsigned(6)) {
    .inverted = !in;
}
```

The component has a six-bit input `in` and a six-bit output
`inverted`. The ouput is set to the bitwise complement of the
input. The details will be found in a later chapter, but to get an
idea, here are the main points:

1. The list of inputs is in the brackets before the arrow `->`, and
   the list of outputs is in the brackets after the arrow.

2. The input `in` and output `inverted` are of type `unsigned(6)`,
   that is, a six-bit unsigned value. In programming languages that
   target typical processors, you can use specific sizes only, such as
   eight bits and 32 bits. In Rox, you can use any size you like.

3. To set `inverted` we use `.inverted` with a dot. This syntax is
   used to write to a value outside the current block, that is,
   outside the closest curly brackets `{` and `}`. Simply writing
   `inverted = !in` would have set an internal signal inside the
   block, but the ouput signal would not be set.

4. Assignment is performed using the `=` operator, and bitwise
   inversion is performed using the `!` operator.

After writing this component, you can generate VHDL from the terminal:

```sh
rox invert.rox
```

This should generate a file called *invert.vhdl*.
