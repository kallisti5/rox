# Introduction

This guide is an introduction to Rox. Rox is a hardware description
language that tries to make it easier to design hardware components.

The `rox` compiler can be used to translate a Rox program to VHDL,
which is supported by most hardware vendors. The current version
generates entity files which can then be used inside external VHDL
code. The generated entities are all synthesizable, so they can be
used for both hardware synthesis and simulation.

Although the aim is that you shouldn't need to learn VHDL or Verilog
if you know Rox, right now you need to know a little VHDL to use Rox
effectively.

## About this guide

Rox is not ready yet, and this guide is similarly incomplete. Some
features described here will change.

This guide assumes some basic knowledge of digital systems and
programming. For example, this guide will not explain unsigned and
two's-complement numbers.
