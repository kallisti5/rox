# Rox

**Rox** is a hardware description language that tries to make it
easier to design hardware components.

Rox is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version. See the full text of the [GNU LGPL][lgpl]
and [GNU GPL][gpl] for details.

The source code for the compiler and for this guide is available on
[GitLab][repo].

## API documentation

Apart from the command-line compiler, rox is accessible through a Rust
[crate]. See the [documentation] for more details.

[documentation]: https://docs.rs/rox/~0.1.6/rox/index.html
[gpl]:  https://www.gnu.org/licenses/gpl-3.0.html
[lgpl]: https://www.gnu.org/licenses/lgpl-3.0.en.html
[repo]: https://gitlab.com/tspiteri/rox
[crate]:  https://crates.io/crates/rox
