# Operators

These are the operators in order of precedence, with the first section
having the highest precedence. All the operators in a single section
have the same precedence.

## Unary operators

Unary operators are the only operators taking a single operand, all
other operators are binary operators with two operands.

   - `!`: Bitwise complement.
   - `+`: Unary plus. This has no effect on its numeric operand.
   - `-`: Unary minus. Negates its numeric operand.
   - `&`: Reducing and. Performs the logic and of all the bits in
     the operand. The result is false if and only if the operand has
     at least one false bit.
   - `|`: Reducing or. Performs the logic or of all the bits in the
     operand. The result is true if and only if the operand has at
     least one true bit.
   - `^`: Reducing xor. Performs the logic exclusive or of all the
     bits in the operand. The result is false when the operand has an
     even number of true bits, and true when the operand has an odd
     number of true bits.

## Cast operator

   - `as`: Converts the left-hand-side operand (LHS) to the type
     specified in the right-hand side (RHS).

## Power operator

   - `**`: Raises the LHS to the power of the RHS.

## Multiplication and division operators

   - `*`: Multiplication.
   - `/`: Division. Dividing two unsized integers yields a rational
     number. Dividing a sized integer truncates the result towards
     zero.
   - `%`: Remainer. The remainder from the division by `/`, such that
     if `d = a / b` and `r = a % b`, then `a == b * d + r`. Since `/`
     rounds towards zero, the sign of `r` is the same as the sign of
     `a`.

These opertions have left-to-right associativity, that is,
`a / b * c == (a / b) * c`.

## Addition operators

   - `+`: Addition.
   - `-`: Subtraction.

## Shifting operators

   - `<<`: Left shift. For unsized integers, rational numbers and
     floating-point numbers, `a << b` is equivalent to `a` multiplied
     by two raised to the power of `b`.
   - `>>`: Right shift. For unsigned numbers this is a logical shift,
     that is, the bits inserted from the left side are zeros. For
     signed numbers this is an arithmetic shift, that is, the bits
     inserted from the left side are the same as the sign bit.

The shift operators can take a negative RHS, such that
`a << b == a >> -b`.

## Bitwise logic and concatenation operators

   - `&`: Bitwise and.
   - `|`: Bitwise or.
   - `^`: Bitwise xor.
   - `!&`: Bitwise nand.
   - `!|`: Bitwise nor.
   - `!^`: Bitwise xnor.
   - `~`: Concatenation.

The bitwise logic and concatenation operators all have the same
precedence, but mixing them is not allowed, you need to use brackets
to mix logic operators. The expressions `a & b & c` and `a ~ b ~ c`
are valid, but the expression `a & b | c` is not valid, it must be
written as either `(a & b) | c` or `a & (b | c)`.

## Relational operators

Unlike arithmetic operations such as addition, relational operators
can have one `signed` operand and one `unsigned` operand, and the
result will be logically correct. The size of the operands can also be
mixed. Comparing a value of type `unsigned(6)` and a value of type
`signed(4)` is equivalent to first converting both of them to
`signed(7)`, which can hold both values exactly, and then comparing
the converted values.

   - `==`: True if LHS is equal to RHS.
   - `!=`: True if LHS is not equal to RHS.
   - `<`: True if LHS is less than to RHS.
   - `<=`: True if LHS is less than or equal to to RHS.
   - `>`: True if LHS is greater than to RHS.
   - `>=`: True if LHS is greater than or equal to RHS.

Relational operators can be chained such that, for example, writing
`a <= b <= c` is equivalent to writing `(a <= b) and (b <= c)`. The
operators do not need to be the same, although the expression
`a <= b > c`, which is equivalent to `(a <= b) and (b > c)`, does not
look as natural as the previous example.
