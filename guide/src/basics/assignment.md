# Assignment

Assignment of signals in Rox is performed using the `=` operator.
You cannot assign to the same signal twice.

```rox
sum = b;
sum = sum + c; // error
```

Assignment statements affect the whole block they are in. This means
that an assigned value can be used in the statements above it as well
as those below it, and the order of assignment statements is not
important.

```rox
a = 12;
sum = a + b; // this works, sum = 25
b = 13;
```

Since Rox is a hardware description language, you can think of
assignment to a signal as connecting a value to a wire. The order in
which the connections are made is not important as long as the right
connections are made.
