# The Basics

Rox has a curly-bracket syntax, with blocks defined using the curly
brackets `{` and `}` like C, Java and Rust. Setting signals also uses
the C-like assignment operator `=`.

This chapter describes the basics of programming in Rox.
