# Comments

Comments in Rox are similar to comments in C. There are two kinds of
comments.

1. Single-line comments start with two slashes `//` and continue to
   the end of the line.

2. Block comments start with `/*` and continue until they are
   terminated with `*/`. Unlike in C, block comments can be nested. In
   the example below, the `*/` next to the inline comment does not
   terminate the comment surrounding the whole block, but only the
   comment started in the same line.
   
   ```rox
   /* disable whole block
   {
       a = b; /* inline comment */
   }
   */
   ```
