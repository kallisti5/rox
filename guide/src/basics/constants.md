# Constants

Constant expressions are evaluated by the Rox compiler during compile
time. There is no need to write a constant modifier, the compiler will
evaluate a constant expression if it is possible.

Rational constants that happen to be integers are automatically
converted to integers. The following works as expected:

```rox
// two_thirds is of type rational
two_thirds = 2 / 3;
// six is of type integer
six = 4 / two_thirds;
// u6 is a type alias of unsigned(6)
u6 = unsigned(six);

// fnum is a floating-point number
fnum = 1.234;
// t is true
t = fnum < 1.2345;
```

Of course, since the order of assignment is not significant, this could
have been written as:

```rox
u6 = unsigned(six);
six = 4 / two_thirds;
two_thirds = 2 / 3;
t = fnum < 1.2345;
fnum = 1.234;
```
