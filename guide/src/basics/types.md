# Types

## Numeric types

Rox supports the following numeric types:
  * `integer` is an unsized integer. An unsized integer can be
    arbitrarily large, but it cannot be written directly to an
    output. However, the conversion from unsized integers to sized
    integers is automatic.
  * `unsigned(n)` is an unsigned number of size `n`, where
    `n`≥0. Addition of unsigned numbers is wrapping addition, that is,
    only the least-significant `n` bits of the answer are retained.
  * `signed(n)` is a signed number of size `n`, where `n`≥0.
  * `rational` is a rational number with arbitrarily large numerator
    and denominator.
  * `float` is a floating-point number that can be used in internal
    expressions.
  
`rational` and `float` cannot be used as inputs or outputs. Currently
there is very little you can do with them. Eventually, they will be
usable to generate constant lookup tables in Rox without needing
external tools.

## Boolean values

Relational expressions in Rox produce a value of type  `bit`. A `bit` value can be either `false` or `true`.

## Type aliases

It is easy to create type aliases in Rox. This statement

```rox
u6 = unsigned(6);
```

creates an alias of the `unsigned(6)` type. The identifier `u6` can be
used anywhere the `unsigned(6)` type specifier can be used. Rox
provides a type alias of its own; `bit` is an alias for `unsigned(1)`,
and `false` and true are constants of type `bit` with values 0 and 1
respectively.

## More comlex types

Eventually, Rox will also support arrays, structure types, and
enumerated types, but they have not been implemented yet.
