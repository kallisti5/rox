# The Rox Programming Language

Rox is a programming language aimed at describing hardware. The
compiler translates source code into synthesizable VHDL.

Rox is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version. See the full text of the [GNU LGPL][lgpl]
and [GNU GPL][gpl] for details.

## Guide

The Rox [guide] gives an overview of the current state of Rox.

The [roadmap] gives an indication of the next features that will be
implemented.

## Building from source

1. Make sure you have installed:

   * `cargo`
   * `git`
   * `rustc`

2. Clone the [source] with `git`:
   ```sh
   git clone https://gitlab.com/tspiteri/rox.git
   cd rox
   ```

3. Build:
   ```sh
   cargo build --release
   ```

4. Install: Once you build, you can copy the generated executable
   `rox` to a directory in your path.

## Running

The `rox` executable will read a list of files and output a VHDL for
each component defined in the files. For more help run:

```sh
rox --help
```

[gpl]:     https://www.gnu.org/licenses/gpl-3.0.html
[guide]:   https://www.rox-lang.org/guide/
[lgpl]:    https://www.gnu.org/licenses/lgpl-3.0.en.html
[roadmap]: https://gitlab.com/tspiteri/rox/blob/master/ROADMAP.md
[source]:  https://gitlab.com/tspiteri/rox
