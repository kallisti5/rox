# Roadmap

This document describes the developement plan for Rox.

## High priority tasks

1. Implement arrays and synchronous memory.

2. Implement structs.

3. Implement enums and FSMs.

4. Implement automatic pipeline generation.

## Lower priority tasks

1. Do not bail out on the first error, but try to generate more error
   messages.
   
2. Write a test suite.

3. Support functions that do not generate VHDL components.

4. Support bidirectional ports.

5. Implement floating-point functions that can be used in constant
   folding.

## Implemented features

1. Check guide at <https://www.rox-lang.org/guide/>.

2. Implemented features not yet in guide:

   * D registers.
