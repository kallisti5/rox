// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use std::env;
use std::fs::{self, ReadDir};
use std::path::{Path, PathBuf};
use std::process::Command;

fn executable() -> PathBuf {
    let test_exe = env::current_exe().expect("current_exe");
    let test_dir = test_exe.parent().expect("parent of current_exe");
    #[cfg(not(windows))]
    let bin_name = "rox";
    #[cfg(windows)]
    let bin_name = "rox.exe";
    let same_dir_exe = test_dir.join(bin_name);
    if same_dir_exe.is_file() {
        same_dir_exe
    } else {
        let up_dir_exe = test_dir
            .parent()
            .expect("parent of parent of current_exe")
            .join(bin_name);
        assert!(up_dir_exe.is_file(), "rox executable not found");
        up_dir_exe
    }
}

fn tests_dir() -> PathBuf {
    let top_dir = env::current_dir().expect("current_dir");
    let tests_dir = top_dir.join("tests");
    assert!(tests_dir.is_dir(), "tests directory not found");
    tests_dir
}

fn files_in_dir(path: &Path, suffix: &'static str) -> Files {
    let entries = fs::read_dir(path).unwrap_or_else(|_| {
        panic!("could not read directory {}", path.display())
    });
    Files { entries, suffix }
}

struct Files {
    entries: ReadDir,
    suffix: &'static str,
}

impl Iterator for Files {
    type Item = PathBuf;
    fn next(&mut self) -> Option<PathBuf> {
        let suffix = self.suffix;
        let entr = &mut self.entries;
        entr.map(|o| o.expect("next directory entry"))
            .filter(|e| e.path().extension().map_or(false, |x| x == suffix))
            .filter(|e| e.file_type().expect("file type").is_file())
            .map(|e| e.path())
            .next()
    }
}

fn test(error: &str, stage: &str, dir: &Path, dump_ext: Option<&str>) {
    let exe = executable();
    for f in files_in_dir(dir, "rox") {
        println!("Testing {} for {}", stage, f.display());
        let mut command = Command::new(&exe);
        command.arg(&f).arg("--stop-after").arg(stage);
        if let Some(extension) = dump_ext {
            command
                .arg(format!("--dump-{}", stage))
                .arg(f.with_extension(extension));
        }
        let output = command.output().expect("failed to execute rox");
        if !output.status.success() {
            println!(
                "{} in {}:\n\
                 Standard output: <<<<<<<<\\\n{}>>>>>>>>\n\
                 Standard error: <<<<<<<<\\\n{}>>>>>>>>\n",
                error,
                f.display(),
                String::from_utf8_lossy(&output.stdout),
                String::from_utf8_lossy(&output.stderr),
            );
            panic!("{} in {}", error, f.display());
        }
    }
}

#[test]
fn parse() {
    let dump = if cfg!(feature = "dump-tests") {
        Some("parse.dump")
    } else {
        None
    };
    let tests_dir = tests_dir();
    for sub in &["parse", "resolve"] {
        test("Parse error", "parse", &tests_dir.join(sub), dump);
    }
    let top_dir = tests_dir.parent().expect("parent directory");
    // do not dump in examples directory
    test("Parse error", "parse", &top_dir.join("examples"), None);
}

#[test]
fn resolve() {
    let dump = if cfg!(feature = "dump-tests") {
        Some("resolve.dump")
    } else {
        None
    };
    let tests_dir = tests_dir();
    test("Resolve error", "resolve", &tests_dir.join("resolve"), dump);
}
