// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use input::{SourceSet, Span};
use parse::TokenKind;
use push_vec::{Index, OptionIndex, PushVec};
use utils;

use rug::float::OrdFloat;
use rug::{Float, Integer};
use std::fmt::{Display, Formatter, Result as FmtResult};

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Node {
    kind: NodeKind,
    span: Span,
}

fn _static_assertions() {
    #[cfg(target_pointer_width = "32")]
    static_assert_size!(Node: 24);
    #[cfg(target_pointer_width = "64")]
    static_assert_size!(Node: 32);
}

impl Node {
    pub fn span(&self) -> Span {
        self.span
    }

    pub fn set_span(&mut self, span: Span) {
        self.span = span;
    }

    pub fn is_stat(&self) -> bool {
        use self::NodeKind::*;
        match self.kind {
            StatAssign { .. }
            | StatCatAssign { .. }
            | StatBlock { .. }
            | StatExpr { .. }
            | StatEvent { .. }
            | StatCond { .. } => true,
            _ => false,
        }
    }

    pub fn is_expr(&self) -> bool {
        use self::NodeKind::*;
        match self.kind {
            Tuple { .. }
            | Record { .. }
            | Variant { .. }
            | Array { .. }
            | Component { .. }
            | Ident { .. }
            | Input
            | Output
            | Integer { .. }
            | IntegerXz { .. }
            | Real { .. }
            | Imaginary { .. }
            | ExprRange { .. }
            | ExprRangeSized { .. }
            | ExprRel { .. }
            | ExprBitwise { .. }
            | ExprConcat { .. }
            | ExprShift { .. }
            | ExprAdd { .. }
            | ExprMul { .. }
            | ExprPow { .. }
            | ExprCast { .. }
            | ExprUnary { .. }
            | ExprField { .. }
            | ExprParam { .. }
            | ExprIndex { .. } => true,
            _ => false,
        }
    }

    pub fn is_pattern(&self) -> bool {
        use self::NodeKind::*;
        match self.kind {
            PatAssign { .. }
            | PatTuple { .. }
            | PatRecord { .. }
            | PatTag { .. }
            | PatExpr { .. } => true,
            _ => false,
        }
    }

    pub fn has_scope(&self) -> bool {
        use self::NodeKind::*;
        match self.kind {
            Root { .. }
            | Package { .. }
            | File { .. }
            | Component { .. }
            | StatBlock { .. } => true,
            _ => false,
        }
    }

    pub fn children(&self) -> ChildrenIter {
        use self::NodeKind::*;
        match &self.kind {
            Root { packages: s }
            | Package { files: s }
            | File { stats: s }
            | Tuple { exprs: s }
            | Record { fields: s }
            | Variant { tags: s }
            | StatBlock { stats: s }
            | StatCond { selectors: s }
            | PatTuple { patterns: s }
            | PatRecord { pat_fields: s } => ChildrenIter(Children::Slice(s)),

            Ident { .. }
            | Input
            | Output
            | Integer { .. }
            | IntegerXz { .. }
            | Real { .. }
            | Imaginary { .. }
            | CatComp
            | CatVariant => ChildrenIter(Children::Slice(&[])),

            Array { ty: i }
            | StatExpr { expr: i }
            | SelectorElse { stat: i }
            | ExprUnary { op: i, .. }
            | PatAssign { ident: i }
            | PatExpr { expr: i } => ChildrenIter(Children::One((*i).into())),

            Field { ident: i, expr: j }
            | Tag { ident: i, expr: j }
            | StatCatAssign { lhs: i, rhs: j }
            | SelectorIf {
                condition: i,
                stat: j,
            }
            | ExprRangeSized {
                begin: i, len: j, ..
            }
            | ExprRel { lhs: i, rhs: j, .. }
            | ExprBitwise { lhs: i, rhs: j, .. }
            | ExprConcat { lhs: i, rhs: j }
            | ExprShift { lhs: i, rhs: j, .. }
            | ExprAdd { lhs: i, rhs: j, .. }
            | ExprMul { lhs: i, rhs: j, .. }
            | ExprPow { lhs: i, rhs: j }
            | ExprCast { lhs: i, rhs: j }
            | ExprField { lhs: i, ident: j }
            | ExprParam { lhs: i, rhs: j }
            | ExprIndex { lhs: i, rhs: j }
            | PatField {
                ident: i,
                pattern: j,
            }
            | PatTag {
                ident: i,
                pattern: j,
            } => ChildrenIter(Children::Two((*i).into(), (*j).into())),

            Component {
                input: i,
                output: j,
                stat: k,
            }
            | StatEvent {
                event: i,
                reg: j,
                val: k,
            } => ChildrenIter(Children::Three(
                (*i).into(),
                (*j).into(),
                (*k).into(),
            )),

            StatAssign { reset, lhs, rhs } => ChildrenIter(Children::Three(
                *reset,
                (*lhs).into(),
                (*rhs).into(),
            )),

            SelectorMatch { sel_and_pats, stat } => {
                ChildrenIter(Children::SliceOne(sel_and_pats, (*stat).into()))
            }
            ExprRange { begin, end, .. } => {
                ChildrenIter(Children::Two((*begin).into(), *end))
            }
        }
    }

    pub fn get_root(&self) -> Option<&[Index]> {
        match &self.kind {
            NodeKind::Root { packages } => Some(packages),
            _ => None,
        }
    }

    pub fn get_package(&self) -> Option<&[Index]> {
        match &self.kind {
            NodeKind::Package { files } => Some(files),
            _ => None,
        }
    }

    pub fn get_file(&self) -> Option<&[Index]> {
        match &self.kind {
            NodeKind::File { stats } => Some(stats),
            _ => None,
        }
    }

    pub fn get_tuple(&self) -> Option<&[Index]> {
        match &self.kind {
            NodeKind::Tuple { exprs } => Some(exprs),
            _ => None,
        }
    }

    pub fn get_record(&self) -> Option<&[Index]> {
        match &self.kind {
            NodeKind::Record { fields } => Some(fields),
            _ => None,
        }
    }

    pub fn get_field(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::Field { ident, expr } => Some((ident, expr)),
            _ => None,
        }
    }

    pub fn get_variant(&self) -> Option<&[Index]> {
        match &self.kind {
            NodeKind::Variant { tags } => Some(tags),
            _ => None,
        }
    }

    pub fn get_tag(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::Tag { ident, expr } => Some((ident, expr)),
            _ => None,
        }
    }

    pub fn get_array(&self) -> Option<Index> {
        match self.kind {
            NodeKind::Array { ty } => Some(ty),
            _ => None,
        }
    }

    pub fn get_component(&self) -> Option<(Index, Index, Index)> {
        match self.kind {
            NodeKind::Component {
                input,
                output,
                stat,
            } => Some((input, output, stat)),
            _ => None,
        }
    }

    pub fn get_stat_assign(&self) -> Option<(Option<Index>, Index, Index)> {
        match self.kind {
            NodeKind::StatAssign { reset, lhs, rhs } => {
                Some((reset.as_option(), lhs, rhs))
            }
            _ => None,
        }
    }

    pub fn get_stat_cat_assign(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::StatCatAssign { lhs, rhs } => Some((lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_stat_block(&self) -> Option<&[Index]> {
        match &self.kind {
            NodeKind::StatBlock { stats } => Some(stats),
            _ => None,
        }
    }

    pub fn get_stat_expr(&self) -> Option<Index> {
        match self.kind {
            NodeKind::StatExpr { expr } => Some(expr),
            _ => None,
        }
    }

    pub fn get_stat_event(&self) -> Option<(Index, Index, Index)> {
        match self.kind {
            NodeKind::StatEvent { event, reg, val } => Some((event, reg, val)),
            _ => None,
        }
    }

    pub fn get_stat_cond(&self) -> Option<&[Index]> {
        match &self.kind {
            NodeKind::StatCond { selectors } => Some(selectors),
            _ => None,
        }
    }

    pub fn get_selector(&self) -> Option<Selector> {
        match &self.kind {
            NodeKind::SelectorIf { condition, stat } => Some(Selector::If {
                condition: *condition,
                stat: *stat,
            }),
            NodeKind::SelectorMatch { sel_and_pats, stat } => {
                Some(Selector::Match {
                    select: sel_and_pats[0],
                    patterns: &sel_and_pats[1..],
                    stat: *stat,
                })
            }
            NodeKind::SelectorElse { stat } => {
                Some(Selector::Else { stat: *stat })
            }
            _ => None,
        }
    }

    pub fn get_ident(&self) -> Option<(&str)> {
        match &self.kind {
            NodeKind::Ident { ident } => Some(ident),
            _ => None,
        }
    }

    pub fn is_input(&self) -> bool {
        self.kind == NodeKind::Input
    }

    pub fn is_output(&self) -> bool {
        self.kind == NodeKind::Output
    }

    pub fn get_integer(&self) -> Option<&Integer> {
        match &self.kind {
            NodeKind::Integer { val } => Some(val),
            _ => None,
        }
    }

    pub fn get_integer_xz(&self) -> Option<(&Integer, &Integer)> {
        match &self.kind {
            NodeKind::IntegerXz { val_mode } => {
                Some((&val_mode.0, &val_mode.1))
            }
            _ => None,
        }
    }

    pub fn get_real(&self) -> Option<&Float> {
        match &self.kind {
            NodeKind::Real { val } => Some(val.as_float()),
            _ => None,
        }
    }

    pub fn get_imaginary(&self) -> Option<&Float> {
        match &self.kind {
            NodeKind::Imaginary { val } => Some(val.as_float()),
            _ => None,
        }
    }

    pub fn get_expr_range(&self) -> Option<(bool, Index, Option<Index>)> {
        match self.kind {
            NodeKind::ExprRange {
                inclusive_end,
                begin,
                end,
            } => Some((inclusive_end, begin, end.into())),
            _ => None,
        }
    }

    pub fn get_expr_range_sized(&self) -> Option<(bool, Index, Index)> {
        match self.kind {
            NodeKind::ExprRangeSized {
                reverse_dir,
                begin,
                len,
            } => Some((reverse_dir, begin, len)),
            _ => None,
        }
    }

    pub fn get_expr_rel(&self) -> Option<(TokenKind, Index, Index)> {
        match self.kind {
            NodeKind::ExprRel { kind, lhs, rhs } => Some((kind, lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_expr_bitwise(&self) -> Option<(TokenKind, Index, Index)> {
        match self.kind {
            NodeKind::ExprBitwise { kind, lhs, rhs } => Some((kind, lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_expr_concat(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::ExprConcat { lhs, rhs } => Some((lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_expr_shift(&self) -> Option<(TokenKind, Index, Index)> {
        match self.kind {
            NodeKind::ExprShift { kind, lhs, rhs } => Some((kind, lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_expr_add(&self) -> Option<(TokenKind, Index, Index)> {
        match self.kind {
            NodeKind::ExprAdd { kind, lhs, rhs } => Some((kind, lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_expr_mul(&self) -> Option<(TokenKind, Index, Index)> {
        match self.kind {
            NodeKind::ExprMul { kind, lhs, rhs } => Some((kind, lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_expr_pow(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::ExprPow { lhs, rhs } => Some((lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_expr_cast(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::ExprCast { lhs, rhs } => Some((lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_expr_unary(&self) -> Option<(TokenKind, Index)> {
        match self.kind {
            NodeKind::ExprUnary { kind, op } => Some((kind, op)),
            _ => return None,
        }
    }

    pub fn get_expr_field(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::ExprField { lhs, ident } => Some((lhs, ident)),
            _ => None,
        }
    }

    pub fn get_expr_param(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::ExprParam { lhs, rhs } => Some((lhs, rhs)),
            _ => None,
        }
    }

    pub fn get_expr_index(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::ExprIndex { lhs, rhs } => Some((lhs, rhs)),
            _ => None,
        }
    }

    pub fn is_cat_comp(&self) -> bool {
        self.kind == NodeKind::CatComp
    }

    pub fn is_cat_variant(&self) -> bool {
        self.kind == NodeKind::CatVariant
    }

    pub fn get_pat_assign(&self) -> Option<Index> {
        match self.kind {
            NodeKind::PatAssign { ident } => Some(ident),
            _ => None,
        }
    }

    pub fn get_pat_tuple(&self) -> Option<&[Index]> {
        match &self.kind {
            NodeKind::PatTuple { patterns } => Some(patterns),
            _ => None,
        }
    }

    pub fn get_pat_record(&self) -> Option<&[Index]> {
        match &self.kind {
            NodeKind::PatRecord { pat_fields } => Some(pat_fields),
            _ => None,
        }
    }

    pub fn get_pat_field(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::PatField { ident, pattern } => Some((ident, pattern)),
            _ => None,
        }
    }

    pub fn get_pat_tag(&self) -> Option<(Index, Index)> {
        match self.kind {
            NodeKind::PatTag { ident, pattern } => Some((ident, pattern)),
            _ => None,
        }
    }

    pub fn get_pat_expr(&self) -> Option<Index> {
        match self.kind {
            NodeKind::PatExpr { expr } => Some(expr),
            _ => None,
        }
    }

    fn get_text(&self) -> String {
        use self::NodeKind::*;
        match &self.kind {
            Root { .. } => "Root".into(),
            Package { .. } => "Package".into(),
            File { .. } => "File".into(),
            Tuple { .. } => "Tuple".into(),
            Record { .. } => "Record".into(),
            Field { .. } => "Field".into(),
            Variant { .. } => "Variant".into(),
            Tag { .. } => "Tag".into(),
            Array { .. } => "Array".into(),
            Component { .. } => "Component".into(),
            StatAssign { .. } => "StatAssign".into(),
            StatCatAssign { .. } => "StatCatAssign".into(),
            StatBlock { .. } => "StatBlock".into(),
            StatExpr { .. } => "StatExpr".into(),
            StatEvent { .. } => "StatEvent".into(),
            StatCond { .. } => "StatCond".into(),
            SelectorIf { .. } => "SelectorIf".into(),
            SelectorMatch { .. } => "SelectorMatch".into(),
            SelectorElse { .. } => "SelectorElse".into(),
            Ident { ident } => format!("Ident({})", ident),
            Input => "Input".into(),
            Output => "Output".into(),
            Integer { val } => format!("Integer({})", val),
            IntegerXz { val_mode } => format!(
                "IntegerXz {{ val: {}, mode: {} }}",
                val_mode.0, val_mode.1
            ),
            Real { val } => format!("Real({})", val.as_float()),
            Imaginary { val } => format!("Imaginary({})", val.as_float()),
            ExprRange { inclusive_end, .. } => {
                format!("ExprRange {{ inclusive_end: {} }}", inclusive_end)
            }
            ExprRangeSized { reverse_dir, .. } => {
                format!("ExprRangeSized {{ reverse_dir: {} }}", reverse_dir)
            }
            ExprRel { kind, .. } => {
                format!("ExprRel {{ kind: {} }}", kind.as_str())
            }
            ExprBitwise { kind, .. } => {
                format!("ExprBitwise {{ kind: {} }}", kind.as_str())
            }
            ExprConcat { .. } => "ExprConcat".into(),
            ExprShift { kind, .. } => {
                format!("ExprShift {{ kind: {} }}", kind.as_str())
            }
            ExprAdd { kind, .. } => {
                format!("ExprAdd {{ kind: {} }}", kind.as_str())
            }
            ExprMul { kind, .. } => {
                format!("ExprMul {{ kind: {} }}", kind.as_str())
            }
            ExprPow { .. } => "ExprPow".into(),
            ExprCast { .. } => "ExprCast".into(),
            ExprUnary { kind, .. } => {
                format!("ExprUnary {{ kind: {} }}", kind.as_str())
            }
            ExprField { .. } => "ExprField".into(),
            ExprParam { .. } => "ExprParam".into(),
            ExprIndex { .. } => "ExprIndex".into(),
            CatComp => "CatComp".into(),
            CatVariant => "CatVariant".into(),
            PatAssign { .. } => "PatAssign".into(),
            PatTuple { .. } => "PatTuple".into(),
            PatRecord { .. } => "PatRecord".into(),
            PatField { .. } => "PatField".into(),
            PatTag { .. } => "PatTag".into(),
            PatExpr { .. } => "PatExpr".into(),
        }
    }
}

impl From<NodeKind> for Node {
    fn from(src: NodeKind) -> Node {
        Node {
            kind: src,
            span: Default::default(),
        }
    }
}

pub struct NodesDisplay<'a> {
    sources: Option<&'a SourceSet>,
    nodes: &'a PushVec<Node>,
    index: Index,
}

pub fn display<'a>(
    sources: Option<&'a SourceSet>,
    nodes: &'a PushVec<Node>,
    index: Index,
) -> NodesDisplay<'a> {
    NodesDisplay {
        sources,
        nodes,
        index,
    }
}

impl<'a> Display for NodesDisplay<'a> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        let mut buf = String::from("\n");
        format(
            self.nodes,
            self.sources,
            self.index.into(),
            FormatKind::Top,
            &mut buf,
            f,
        )
    }
}

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
enum NodeKind {
    Root {
        packages: Box<[Index]>,
    },
    Package {
        files: Box<[Index]>,
    },
    File {
        stats: Box<[Index]>,
    },

    Tuple {
        exprs: Box<[Index]>,
    },
    Record {
        fields: Box<[Index]>,
    },
    Field {
        ident: Index,
        expr: Index,
    },
    Variant {
        tags: Box<[Index]>,
    },
    Tag {
        ident: Index,
        expr: Index,
    },
    Array {
        ty: Index,
    },

    Component {
        input: Index,
        output: Index,
        stat: Index,
    },
    StatAssign {
        reset: OptionIndex,
        lhs: Index,
        rhs: Index,
    },
    StatCatAssign {
        lhs: Index,
        rhs: Index,
    },
    StatBlock {
        stats: Box<[Index]>,
    },
    StatExpr {
        expr: Index,
    },
    StatEvent {
        event: Index,
        reg: Index,
        val: Index,
    },
    StatCond {
        selectors: Box<[Index]>,
    },
    SelectorIf {
        condition: Index,
        stat: Index,
    },
    SelectorMatch {
        sel_and_pats: Box<[Index]>,
        stat: Index,
    },
    SelectorElse {
        stat: Index,
    },

    Ident {
        ident: Box<str>,
    },
    Input,
    Output,
    #[cfg(target_pointer_width = "64")]
    Integer {
        val: Integer,
    },
    #[cfg(target_pointer_width = "32")]
    Integer {
        val: Box<Integer>,
    },
    // x bit (don't care) = val 0 mode 1
    // z bit (high impedance) = val 1 mode 1
    IntegerXz {
        val_mode: Box<(Integer, Integer)>,
    },
    Real {
        val: Box<OrdFloat>,
    },
    Imaginary {
        val: Box<OrdFloat>,
    },

    ExprRange {
        inclusive_end: bool,
        begin: Index,
        end: OptionIndex,
    },
    ExprRangeSized {
        reverse_dir: bool,
        begin: Index,
        len: Index,
    },
    ExprRel {
        kind: TokenKind,
        lhs: Index,
        rhs: Index,
    },
    ExprBitwise {
        kind: TokenKind,
        lhs: Index,
        rhs: Index,
    },
    ExprConcat {
        lhs: Index,
        rhs: Index,
    },
    ExprShift {
        kind: TokenKind,
        lhs: Index,
        rhs: Index,
    },
    ExprAdd {
        kind: TokenKind,
        lhs: Index,
        rhs: Index,
    },
    ExprMul {
        kind: TokenKind,
        lhs: Index,
        rhs: Index,
    },
    ExprPow {
        lhs: Index,
        rhs: Index,
    },
    ExprCast {
        lhs: Index,
        rhs: Index,
    },
    ExprUnary {
        kind: TokenKind,
        op: Index,
    },
    ExprField {
        lhs: Index,
        ident: Index,
    },
    ExprParam {
        lhs: Index,
        rhs: Index,
    },
    ExprIndex {
        lhs: Index,
        rhs: Index,
    },

    CatComp,
    CatVariant,

    PatAssign {
        ident: Index,
    },
    PatTuple {
        patterns: Box<[Index]>,
    },
    PatRecord {
        pat_fields: Box<[Index]>,
    },
    PatField {
        ident: Index,
        pattern: Index,
    },
    PatTag {
        ident: Index,
        pattern: Index,
    },
    PatExpr {
        expr: Index,
    },
}

macro_rules! assert_all {
    ($slice:expr, $nodes:expr, $get:ident, $($panic:tt)*) => {
        for &i in $slice.iter() {
            assert!($nodes[i].$get().is_some(), $($panic)*);
        }
    };
}

macro_rules! assert_all_is {
    ($slice:expr, $nodes:expr, $is:ident, $($panic:tt)*) => {
        for &i in $slice.iter() {
            assert!($nodes[i].$is(), $($panic)*);
        }
    };
}

pub fn root(packages: Box<[Index]>, nodes: &mut PushVec<Node>) -> Index {
    assert_all!(packages, nodes, get_package, "expected package");
    nodes.push(NodeKind::Root { packages })
}

pub fn package(files: Box<[Index]>, nodes: &mut PushVec<Node>) -> Index {
    assert_all!(files, nodes, get_file, "expected file");
    nodes.push(NodeKind::Package { files })
}

pub fn file(stats: Box<[Index]>, nodes: &mut PushVec<Node>) -> Index {
    assert_all_is!(stats, nodes, is_stat, "expected statement");
    nodes.push(NodeKind::File { stats })
}

pub fn tuple(exprs: Box<[Index]>, nodes: &mut PushVec<Node>) -> Index {
    assert_all_is!(exprs, nodes, is_expr, "expected expression");
    nodes.push(NodeKind::Tuple { exprs })
}

pub fn record(fields: Box<[Index]>, nodes: &mut PushVec<Node>) -> Index {
    assert_all!(fields, nodes, get_field, "expected field");
    nodes.push(NodeKind::Record { fields })
}

pub fn field(ident: Index, expr: Index, nodes: &mut PushVec<Node>) -> Index {
    nodes[ident].get_ident().expect("expected identifier");
    assert!(nodes[expr].is_expr(), "expected expression");
    nodes.push(NodeKind::Field { ident, expr })
}

pub fn variant(tags: Box<[Index]>, nodes: &mut PushVec<Node>) -> Index {
    assert_all!(tags, nodes, get_tag, "expected tag");
    nodes.push(NodeKind::Variant { tags })
}

pub fn tag(ident: Index, expr: Index, nodes: &mut PushVec<Node>) -> Index {
    nodes[ident].get_ident().expect("expected identifier");
    assert!(nodes[expr].is_expr(), "expected expression");
    nodes.push(NodeKind::Tag { ident, expr })
}

pub fn array(ty: Index, nodes: &mut PushVec<Node>) -> Index {
    assert!(nodes[ty].is_expr(), "expected expression");
    nodes.push(NodeKind::Array { ty })
}

pub fn component(
    input: Index,
    output: Index,
    stat: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(
        nodes[input].is_expr() || nodes[input].is_pattern(),
        "expected expression or pattern"
    );
    assert!(nodes[output].is_expr(), "expected expression");
    nodes[stat]
        .get_stat_block()
        .expect("expected block statement");
    nodes.push(NodeKind::Component {
        input,
        output,
        stat,
    })
}

pub fn stat_assign(
    lhs: Index,
    rhs: Index,
    reset: Option<Index>,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(
        nodes[rhs].is_expr()
            || nodes[rhs].kind == NodeKind::CatComp
            || nodes[rhs].kind == NodeKind::CatVariant,
        "expected expression or concatenation"
    );
    if let Some(reset) = reset {
        assert!(nodes[reset].is_expr(), "expected expression");
    }
    let reset = reset.into();
    nodes.push(NodeKind::StatAssign { lhs, rhs, reset })
}

pub fn stat_cat_assign(
    lhs: Index,
    rhs: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    nodes[lhs].get_ident().expect("expected identifier");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::StatCatAssign { lhs, rhs })
}

pub fn stat_block(stats: Box<[Index]>, nodes: &mut PushVec<Node>) -> Index {
    assert_all_is!(stats, nodes, is_stat, "expected statement");
    nodes.push(NodeKind::StatBlock { stats })
}

pub fn stat_expr(expr: Index, nodes: &mut PushVec<Node>) -> Index {
    assert!(nodes[expr].is_expr(), "expected expression");
    nodes.push(NodeKind::StatExpr { expr })
}

pub fn stat_event(
    event: Index,
    reg: Index,
    val: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[event].is_expr(), "expected expression");
    assert!(nodes[reg].is_expr(), "expected expression");
    assert!(nodes[val].is_expr(), "expected expression");
    nodes.push(NodeKind::StatEvent { event, reg, val })
}

pub fn stat_cond(selectors: Box<[Index]>, nodes: &mut PushVec<Node>) -> Index {
    assert_all!(selectors, nodes, get_selector, "expected selector");
    nodes.push(NodeKind::StatCond { selectors })
}

pub fn selector_if(
    condition: Index,
    stat: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[condition].is_expr(), "expected expression");
    nodes[stat]
        .get_stat_block()
        .expect("expected block statement");
    nodes.push(NodeKind::SelectorIf { condition, stat })
}

pub fn selector_match(
    select: Index,
    patterns: Box<[Index]>,
    stat: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[select].is_expr(), "expected expression");
    assert_all_is!(patterns, nodes, is_pattern, "expected pattern");
    nodes[stat]
        .get_stat_block()
        .expect("expected block statement");
    let mut sel_and_pats = Vec::with_capacity(1 + patterns.len());
    sel_and_pats.push(select);
    sel_and_pats.extend(patterns.into_iter());
    let sel_and_pats = sel_and_pats.into_boxed_slice();
    nodes.push(NodeKind::SelectorMatch { sel_and_pats, stat })
}

pub fn selector_else(stat: Index, nodes: &mut PushVec<Node>) -> Index {
    nodes[stat]
        .get_stat_block()
        .expect("expected block statement");
    nodes.push(NodeKind::SelectorElse { stat })
}

pub enum Selector<'a> {
    If {
        condition: Index,
        stat: Index,
    },
    Match {
        select: Index,
        patterns: &'a [Index],
        stat: Index,
    },
    Else {
        stat: Index,
    },
}

pub fn ident(ident: Box<str>, nodes: &mut PushVec<Node>) -> Index {
    nodes.push(NodeKind::Ident { ident })
}

pub fn input(nodes: &mut PushVec<Node>) -> Index {
    nodes.push(NodeKind::Input)
}

pub fn output(nodes: &mut PushVec<Node>) -> Index {
    nodes.push(NodeKind::Output)
}

pub fn integer(val: Integer, nodes: &mut PushVec<Node>) -> Index {
    #[cfg(target_pointer_width = "32")]
    let val = Box::new(val);
    nodes.push(NodeKind::Integer { val })
}

pub fn integer_xz(
    val: Integer,
    mode: Integer,
    nodes: &mut PushVec<Node>,
) -> Index {
    let val_mode = Box::new((val, mode));
    nodes.push(NodeKind::IntegerXz { val_mode })
}

pub fn real(val: Float, nodes: &mut PushVec<Node>) -> Index {
    let val = Box::new(val.into());
    nodes.push(NodeKind::Real { val })
}

pub fn imaginary(val: Float, nodes: &mut PushVec<Node>) -> Index {
    let val = Box::new(val.into());
    nodes.push(NodeKind::Imaginary { val })
}

pub fn expr_range(
    inclusive_end: bool,
    begin: Index,
    end: Option<Index>,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[begin].is_expr(), "expected expression");
    if let Some(end) = end {
        assert!(nodes[end].is_expr(), "expected expression");
    }
    let end = end.into();
    nodes.push(NodeKind::ExprRange {
        inclusive_end,
        begin,
        end,
    })
}

pub fn expr_range_sized(
    reverse_dir: bool,
    begin: Index,
    len: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[begin].is_expr(), "expected expression");
    assert!(nodes[len].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprRangeSized {
        reverse_dir,
        begin,
        len,
    })
}

pub fn expr_rel(
    kind: TokenKind,
    lhs: Index,
    rhs: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprRel { kind, lhs, rhs })
}

pub fn expr_bitwise(
    kind: TokenKind,
    lhs: Index,
    rhs: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprBitwise { kind, lhs, rhs })
}

pub fn expr_concat(lhs: Index, rhs: Index, nodes: &mut PushVec<Node>) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprConcat { lhs, rhs })
}

pub fn expr_shift(
    kind: TokenKind,
    lhs: Index,
    rhs: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprShift { kind, lhs, rhs })
}

pub fn expr_add(
    kind: TokenKind,
    lhs: Index,
    rhs: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprAdd { kind, lhs, rhs })
}

pub fn expr_mul(
    kind: TokenKind,
    lhs: Index,
    rhs: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprMul { kind, lhs, rhs })
}

pub fn expr_pow(lhs: Index, rhs: Index, nodes: &mut PushVec<Node>) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprPow { lhs, rhs })
}

pub fn expr_cast(lhs: Index, rhs: Index, nodes: &mut PushVec<Node>) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprCast { lhs, rhs })
}

pub fn expr_unary(
    kind: TokenKind,
    op: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[op].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprUnary { kind, op })
}

pub fn expr_field(
    lhs: Index,
    ident: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    nodes[ident].get_ident().expect("expected identifier");
    nodes.push(NodeKind::ExprField { lhs, ident })
}

pub fn expr_param(lhs: Index, rhs: Index, nodes: &mut PushVec<Node>) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprParam { lhs, rhs })
}

pub fn expr_index(lhs: Index, rhs: Index, nodes: &mut PushVec<Node>) -> Index {
    assert!(nodes[lhs].is_expr(), "expected expression");
    assert!(nodes[rhs].is_expr(), "expected expression");
    nodes.push(NodeKind::ExprIndex { lhs, rhs })
}

pub fn cat_comp(nodes: &mut PushVec<Node>) -> Index {
    nodes.push(NodeKind::CatComp)
}

pub fn cat_variant(nodes: &mut PushVec<Node>) -> Index {
    nodes.push(NodeKind::CatVariant)
}

pub fn pat_assign(ident: Index, nodes: &mut PushVec<Node>) -> Index {
    nodes[ident].get_ident().expect("expected identifier");
    nodes.push(NodeKind::PatAssign { ident })
}

pub fn pat_tuple(patterns: Box<[Index]>, nodes: &mut PushVec<Node>) -> Index {
    assert_all_is!(patterns, nodes, is_pattern, "expected pattern");
    nodes.push(NodeKind::PatTuple { patterns })
}

pub fn pat_record(
    pat_fields: Box<[Index]>,
    nodes: &mut PushVec<Node>,
) -> Index {
    assert_all!(pat_fields, nodes, get_pat_field, "expected field");
    nodes.push(NodeKind::PatRecord { pat_fields })
}

pub fn pat_field(
    ident: Index,
    pattern: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    nodes[ident].get_ident().expect("expected identifier");
    assert!(nodes[pattern].is_pattern(), "expected pattern");
    nodes.push(NodeKind::PatField { ident, pattern })
}

pub fn pat_tag(
    ident: Index,
    pattern: Index,
    nodes: &mut PushVec<Node>,
) -> Index {
    nodes[ident].get_ident().expect("expected identifier");
    assert!(nodes[pattern].is_pattern(), "expected pattern");
    nodes.push(NodeKind::PatTag { ident, pattern })
}

pub fn pat_expr(expr: Index, nodes: &mut PushVec<Node>) -> Index {
    assert!(nodes[expr].is_expr(), "expected expression");
    nodes.push(NodeKind::PatExpr { expr })
}

enum Children<'a> {
    Slice(&'a [Index]),
    One(OptionIndex),
    Two(OptionIndex, OptionIndex),
    Three(OptionIndex, OptionIndex, OptionIndex),
    SliceOne(&'a [Index], OptionIndex),
}

pub struct ChildrenIter<'a>(Children<'a>);

impl<'a> Iterator for ChildrenIter<'a> {
    type Item = OptionIndex;
    fn next(&mut self) -> Option<OptionIndex> {
        let (child, rem) = match self.0 {
            Children::Slice(slice) => {
                if let Some(s) = slice.get(0) {
                    ((*s).into(), Children::Slice(&slice[1..]))
                } else {
                    return None;
                }
            }
            Children::One(i) => (i, Children::Slice(&[])),
            Children::Two(i, j) => (i, Children::One(j)),
            Children::Three(i, j, k) => (i, Children::Two(j, k)),
            Children::SliceOne(slice, last) => {
                if let Some(s) = slice.get(0) {
                    ((*s).into(), Children::SliceOne(&slice[1..], last))
                } else {
                    (last, Children::Slice(&[]))
                }
            }
        };
        self.0 = rem;
        Some(child)
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = match self.0 {
            Children::Slice(s) => s.len(),
            Children::One(_) => 1,
            Children::Two(_, _) => 2,
            Children::Three(_, _, _) => 3,
            Children::SliceOne(s, _) => 1 + s.len(),
        };
        (size, Some(size))
    }
}

enum FormatKind {
    Top,
    HasSiblings,
    LastChild,
}

fn format(
    nodes: &PushVec<Node>,
    source_set: Option<&SourceSet>,
    index: OptionIndex,
    kind: FormatKind,
    prefix: &mut String,
    f: &mut Formatter,
) -> FmtResult {
    let original_prefix_len = prefix.len();
    let self_prefix = match kind {
        FormatKind::Top => "",
        FormatKind::HasSiblings => "├── ",
        FormatKind::LastChild => "└── ",
    };

    let index = match index.as_option() {
        Some(index) => index,
        None => return write!(f, "{}{}-\n", &prefix[1..], self_prefix),
    };

    let mut text = nodes[index].get_text();
    if let Some(sources) = source_set {
        let span_text = sources.get(nodes[index].span);
        if !span_text.is_empty() {
            text.push_str("\n\"");
            text.push_str(&utils::squash_string(span_text, 32));
            text.push_str("\"");
        }
    }
    write!(f, "{}{}{}: ", &prefix[1..], self_prefix, index)?;
    prefix.push_str(match kind {
        FormatKind::Top => "",
        FormatKind::HasSiblings => "│   ",
        FormatKind::LastChild => "    ",
    });

    let mut children = nodes[index].children().peekable();
    let children_prefix_len = prefix.len();
    prefix.push_str(match children.peek() {
        Some(_) => "│       ",
        None => "        ",
    });
    let s = text.replace("\n", prefix);
    f.write_str(&s)?;
    f.write_str("\n")?;
    prefix.truncate(children_prefix_len);
    while let Some(child) = children.next() {
        let child_kind = match children.peek() {
            Some(_) => FormatKind::HasSiblings,
            None => FormatKind::LastChild,
        };
        format(nodes, source_set, child, child_kind, prefix, f)?;
    }

    prefix.truncate(original_prefix_len);
    Ok(())
}
