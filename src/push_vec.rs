// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use std::fmt::{Debug, Display, Formatter, Result as FmtResult};
use std::ops::{Index as OpsIndex, IndexMut};
use std::slice::Iter as SliceIter;

#[derive(Clone, Copy, Eq, Hash, Ord, PartialEq, PartialOrd)]
// must never be !0
pub struct Index(u32);

impl Debug for Index {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        Debug::fmt(&self.0, f)
    }
}

fn _static_assertions() {
    // usize must be 32 or 64 bits wide
    static_assert!(
        cfg!(target_pointer_width = "32") != cfg!(target_pointer_width = "64")
    );
}

impl Index {
    #[inline]
    pub fn as_u32(self) -> u32 {
        self.0
    }
}

#[derive(Clone, Copy, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct OptionIndex {
    optional: u32,
}

impl Debug for OptionIndex {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match &self.as_option() {
            Some(i) => Debug::fmt(i, f),
            None => Debug::fmt(&Option::None::<Index>, f),
        }
    }
}

impl From<OptionIndex> for Option<Index> {
    fn from(src: OptionIndex) -> Option<Index> {
        src.as_option()
    }
}

impl OptionIndex {
    #[inline]
    pub fn as_option(&self) -> Option<Index> {
        if self.optional == !0 {
            None
        } else {
            Some(Index(self.optional))
        }
    }
}

impl PartialEq<Index> for OptionIndex {
    #[inline]
    fn eq(&self, other: &Index) -> bool {
        self.optional == other.0
    }
}

impl PartialEq<OptionIndex> for Index {
    #[inline]
    fn eq(&self, other: &OptionIndex) -> bool {
        self.0 == other.optional
    }
}

impl From<u32> for OptionIndex {
    #[inline]
    fn from(src: u32) -> OptionIndex {
        OptionIndex { optional: src }
    }
}

impl From<Index> for OptionIndex {
    #[inline]
    fn from(src: Index) -> OptionIndex {
        OptionIndex { optional: src.0 }
    }
}

impl From<Option<Index>> for OptionIndex {
    #[inline]
    fn from(src: Option<Index>) -> OptionIndex {
        match src {
            Some(index) => index.into(),
            None => OptionIndex { optional: !0 },
        }
    }
}

impl Display for Index {
    #[inline]
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        Display::fmt(&self.0, f)
    }
}

#[derive(Default)]
pub struct PushVec<T>(Vec<T>);

impl<T> PushVec<T> {
    #[inline]
    pub fn new() -> PushVec<T> {
        PushVec(Vec::new())
    }

    #[inline]
    pub fn from_elem<U>(elem: U, size: Index) -> PushVec<T>
    where
        U: Into<T>,
        T: Clone,
    {
        PushVec(vec![elem.into(); size.as_u32() as usize])
    }

    #[inline]
    pub fn len(&self) -> Index {
        OptionIndex::from(self.0.len() as u32)
            .as_option()
            .expect("capacity overflow")
    }

    #[inline]
    pub fn push<U>(&mut self, elem: U) -> Index
    where
        U: Into<T>,
    {
        let prev_len = self.0.len();
        assert!(prev_len < !0u32 as usize - 1, "capacity overflow");
        self.0.push(elem.into());
        OptionIndex::from(prev_len as u32).as_option().unwrap()
    }

    #[inline]
    pub fn iter(&self) -> SliceIter<T> {
        self.0.iter()
    }
}

impl<T> Debug for PushVec<T>
where
    T: Debug,
{
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        Debug::fmt(&self.0, f)
    }
}

impl<T> OpsIndex<Index> for PushVec<T> {
    type Output = T;

    #[inline]
    fn index(&self, index: Index) -> &T {
        self.0.index(index.as_u32() as usize)
    }
}

impl<T> IndexMut<Index> for PushVec<T> {
    #[inline]
    fn index_mut(&mut self, index: Index) -> &mut T {
        self.0.index_mut(index.as_u32() as usize)
    }
}
