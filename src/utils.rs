// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

pub fn squash_string(chars: &str, width: u32) -> String {
    let mut front = String::new();
    let mut back = String::new();
    let mut front_count = 0;
    let mut back_count = 0;
    let mut prev_is_space = false;
    for c in chars.chars() {
        front_count += 1;
        match c {
            ' ' | '\n' | '\t' => if !prev_is_space {
                front.push(' ');
                prev_is_space = true;
            } else {
                front_count -= 1;
            },
            _ => {
                front.push(c);
                prev_is_space = false;
            }
        }
        if front_count > width {
            prev_is_space = false;
            for c in chars.chars().rev() {
                back_count += 1;
                match c {
                    ' ' | '\n' | '\t' => if !prev_is_space {
                        back.push(' ');
                        prev_is_space = true;
                    } else {
                        back_count -= 1;
                    },
                    _ => {
                        back.push(c);
                        prev_is_space = false;
                    }
                }
                if back_count == 5 {
                    break;
                }
            }
            break;
        }
    }
    if front_count > width {
        let i = front.char_indices().nth(22).expect("22nd index").0;
        front.truncate(i);
        front.push_str(" ... ");
        front.extend(back.chars().rev());
    }
    front
}
