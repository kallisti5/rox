// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use map::{OrderMap, OrderMapIter};
use push_vec::{Index, OptionIndex};

use std::fmt::{Debug, Formatter, Result as FmtResult};

#[derive(Clone)]
struct LazyMap(Option<OrderMap<str, Index>>);

impl Debug for LazyMap {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match &self.0 {
            None => f.debug_map().finish(),
            Some(map) => Debug::fmt(map, f),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Scope {
    outer: OptionIndex,
    map: LazyMap,
}

impl Scope {
    pub fn new(outer: Option<Index>) -> Scope {
        Scope {
            outer: outer.into(),
            map: LazyMap(None),
        }
    }

    pub fn insert(&mut self, id: Box<str>, net: Index) -> Option<Index> {
        match &mut self.map.0 {
            Some(map) => map.insert(id, net),
            r @ None => {
                let mut map = OrderMap::new();
                map.insert(id, net);
                *r = Some(map);
                None
            }
        }
    }

    pub fn get(&self, id: &str) -> Option<Index> {
        match &self.map.0 {
            Some(map) => map.get(id).cloned(),
            None => None,
        }
    }

    pub fn outer(&self) -> Option<Index> {
        self.outer.as_option()
    }

    pub fn iter(&self) -> ScopeIter {
        match &self.map.0 {
            Some(map) => ScopeIter { iter: map.iter() },
            None => Default::default(),
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct ScopeIter<'a> {
    iter: OrderMapIter<'a, str, Index>,
}

impl<'a> Iterator for ScopeIter<'a> {
    type Item = (&'a str, Index);
    #[inline]
    fn next(&mut self) -> Option<(&'a str, Index)> {
        self.iter.next().map(|(s, i)| (s, *i))
    }
}

#[derive(Clone, Debug)]
pub enum Net {
    Const {
        tree_def: Index,
        ty: Type,
    },
    Wire {
        tree_def: Index,
        ty: Type,
    },
    Register {
        net_reset: Index,
        tree_reset_cond: Index,
        net_data: Index,
        tree_event: Index,
    },
}

// primitives and compound types
#[derive(Clone, Debug)]
pub enum Type {
    Bit,
    Integer,
    Rational,
    Unsigned { size: u32 },
    Signed { size: u32 },
    Float { prec: u32 },
    Complex { prec: u32 },
    Record(Record),
    Variant(Variant),
    Array(Array),
    Type(TypeType),
    Comp(Comp),
    MatchComp(MatchComp),
    MatchVariant(MatchVariant),
}

#[derive(Clone, Debug)]
pub struct Record {}

#[derive(Clone, Debug)]
pub struct Variant {}

#[derive(Clone, Debug)]
pub struct Array {}

#[derive(Clone, Debug)]
pub struct TypeType {}

#[derive(Clone, Debug)]
pub struct Comp {}

#[derive(Clone, Debug)]
pub struct MatchComp {}

#[derive(Clone, Debug)]
pub struct MatchVariant {}
