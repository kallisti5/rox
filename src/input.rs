// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use error::Error;

use std::cmp;
use std::fs;
use std::io::{self, Read};
use std::ops::{Add, AddAssign};
use std::u32;

#[derive(Clone, Debug, Hash, Eq, Ord, PartialEq, PartialOrd)]
pub struct SourceSet {
    contents: String,
    filenames: String,
    cum_lens: Vec<CumLen>,
}

impl Default for SourceSet {
    fn default() -> SourceSet {
        SourceSet::new()
    }
}

#[derive(Clone, Copy, Debug, Hash, Eq, Ord, PartialEq, PartialOrd)]
struct CumLen {
    content: u32,
    filename: u32,
}

#[derive(Clone, Copy, Debug, Hash, Eq, Ord, PartialEq, PartialOrd)]
pub struct Span {
    start: u32,
    end: u32,
}

#[derive(Clone, Copy, Debug, Hash, Eq, Ord, PartialEq, PartialOrd)]
pub struct Sources<'a> {
    set: &'a SourceSet,
    index: usize,
}

impl<'a> Iterator for Sources<'a> {
    type Item = (Span, Span);
    fn next(&mut self) -> Option<(Span, Span)> {
        let i = self.index;
        if i >= self.set.num_files() {
            return None;
        }
        self.index += 1;

        let start = if i == 0 {
            (1, 1)
        } else {
            let l = self.set.cum_lens[i - 1];
            (l.content + 1, l.filename + 1)
        };
        let end = self.set.cum_lens[i];
        let s = Span {
            start: start.0,
            end: end.content,
        };
        let f = Span {
            start: start.1,
            end: end.filename,
        };
        Some((s, f))
    }
}

fn file_len(filename: &str) -> Result<u64, Error> {
    let metadata = fs::metadata(filename).map_err(|e| Error::io(filename, e))?;
    if metadata.is_file() {
        Ok(metadata.len())
    } else {
        Err(Error::general(&format!("{}: not a file", filename)))
    }
}

impl SourceSet {
    pub fn new() -> SourceSet {
        SourceSet {
            filenames: String::new(),
            contents: String::new(),
            cum_lens: Vec::new(),
        }
    }

    pub fn num_files(&self) -> usize {
        self.cum_lens.len()
    }

    pub fn read(&mut self, filenames: &[&str]) -> Result<(), Error> {
        let cum_lens = match self.cum_lens.last() {
            None => (1, 1),
            Some(lens) => (lens.content, lens.filename),
        };
        let mut contents_len = 0;
        let mut filenames_len = 0;
        for filename in filenames {
            let f = file_len(filename)?;
            contents_len += 1 + f;
            filenames_len += 1 + filename.len();
        }
        if contents_len > u64::from(u32::MAX - cum_lens.0) {
            Err(Error::general(
                "files are too long, can only translate 4 GiB of code",
            ))?
        }
        if filenames_len > (u32::MAX - cum_lens.1) as usize {
            Err(Error::general("filenames are too long, over 4 GiB!"))?
        }
        self.contents.reserve_exact(contents_len as usize);
        self.filenames.reserve_exact(filenames_len);
        self.cum_lens.reserve_exact(filenames.len());
        for filename in filenames {
            self.read_file(filename)?
        }
        Ok(())
    }

    /// Returns an iterator with Item = (Span, Span), the spans for
    /// the content and for the filename.
    pub fn sources(&self) -> Sources {
        Sources {
            set: self,
            index: 0,
        }
    }

    fn read_file(&mut self, filename: &str) -> Result<(), Error> {
        self.contents.push('\0');
        let file =
            fs::File::open(filename).map_err(|e| Error::io(filename, e))?;
        let mut reader = io::BufReader::new(file);
        reader
            .read_to_string(&mut self.contents)
            .map_err(|e| Error::io(filename, e))?;
        if self.contents.len() > u32::MAX as usize {
            Err(Error::general(
                "files are too long, can only translate 4 GiB of code",
            ))?
        }

        self.filenames.push('\0');
        self.filenames.push_str(filename);
        if self.filenames.len() > u32::MAX as usize {
            Err(Error::general("filenames are too long, over 4 GiB!"))?
        }

        self.cum_lens.push(CumLen {
            content: self.contents.len() as u32,
            filename: self.filenames.len() as u32,
        });
        Ok(())
    }

    fn content(&self, start: u32, end: u32) -> &str {
        &self.contents[start as usize..end as usize]
    }

    fn filename(&self, start: u32, end: u32) -> &str {
        &self.filenames[start as usize..end as usize]
    }

    fn file_index(&self, offset: u32) -> Option<usize> {
        if offset == 0 {
            None
        } else {
            match self.cum_lens.binary_search_by(|l| l.content.cmp(&offset)) {
                Ok(index) | Err(index) => Some(index),
            }
        }
    }

    fn details(&self, content: Span) -> Option<SpanDetails> {
        let index = self.file_index(content.start)?;
        let start = if index == 0 {
            (1, 1)
        } else {
            let l = self.cum_lens[index - 1];
            (l.content + 1, l.filename + 1)
        };
        let end = self.cum_lens[index];
        let (mut line, mut column) = (1, 1);
        for c in self.contents[start.0 as usize..content.start as usize].chars()
        {
            if c == '\n' {
                line += 1;
                column = 1;
            } else {
                column += 1;
            }
        }
        Some(SpanDetails {
            contents: self.content(content.start, content.end),
            filename: self.filename(start.1, end.filename),
            line,
            column,
            eof: content.start == end.content,
        })
    }

    pub fn loc_prefix(&self, content: Span) -> String {
        self.details(content)
            .map(|d| format!("{}:{}:{}: ", d.filename, d.line, d.column))
            .unwrap_or_default()
    }

    pub fn get(&self, content: Span) -> &str {
        self.content(content.start, content.end)
    }

    pub fn get_filename(&self, filename: Span) -> &str {
        self.filename(filename.start, filename.end)
    }

    pub fn skip_whitespace(&self, content: &mut Span) {
        let stream = self.content(content.start, content.end);
        enum CommentState {
            Outside,
            Slash(usize), // position of the slash in the stream
            SlashSlash,
            SlashStar(usize),      // nesting level
            SlashStarStar(usize),  // nesting level
            SlashStarSlash(usize), // nesting level
        };
        let mut comment_state = CommentState::Outside;
        for (index, c) in stream.char_indices() {
            comment_state = match comment_state {
                CommentState::Outside => match c {
                    '\n' | '\t' | '\r' | ' ' => comment_state,
                    '/' => CommentState::Slash(index),
                    _ => {
                        content.start += index as u32;
                        return;
                    }
                },
                CommentState::Slash(pos) => match c {
                    '/' => CommentState::SlashSlash,
                    '*' => CommentState::SlashStar(0),
                    _ => {
                        content.start += pos as u32;
                        return;
                    }
                },
                CommentState::SlashSlash => match c {
                    '\n' => CommentState::Outside,
                    _ => comment_state,
                },
                CommentState::SlashStar(nesting) => match c {
                    '/' => CommentState::SlashStarSlash(nesting),
                    '*' => CommentState::SlashStarStar(nesting),
                    _ => comment_state,
                },
                CommentState::SlashStarSlash(nesting) => match c {
                    '/' => comment_state,
                    '*' => CommentState::SlashStar(nesting + 1),
                    _ => CommentState::SlashStar(nesting),
                },
                CommentState::SlashStarStar(nesting) => match c {
                    '/' if nesting == 0 => CommentState::Outside,
                    '/' => CommentState::SlashStar(nesting - 1),
                    '*' => comment_state,
                    _ => CommentState::SlashStar(nesting),
                },
            }
        }
        if let CommentState::Slash(pos) = comment_state {
            content.start += pos as u32;
        } else {
            content.start = content.end;
        }
    }

    pub fn err(&self, content: Span, message: &str) -> Error {
        let details = match self.details(content) {
            Some(d) => d,
            None => return Error::parse(message),
        };
        let found_buf;
        let found = if details.eof {
            "end of file"
        } else {
            found_buf = format!("'{}'", details.contents);
            &found_buf
        };
        Error::parse(&format!(
            "{}:{}:{} Found {}, {}",
            details.filename, details.line, details.column, found, message
        ))
    }
}

struct SpanDetails<'a> {
    contents: &'a str,
    filename: &'a str,
    line: u32,
    column: u32,
    eof: bool,
}

impl Default for Span {
    fn default() -> Span {
        Span { start: 0, end: 0 }
    }
}

impl Span {
    pub fn advance_start(&mut self, bytes: u32) {
        self.start += bytes;
        if self.end < self.start {
            self.end = self.start;
        }
    }

    pub fn is_empty(&self) -> bool {
        self.start >= self.end
    }

    pub fn len(&self) -> u32 {
        self.end - self.start
    }

    pub fn set_len(&mut self, bytes: u32) {
        self.end = self.start.checked_add(bytes).expect("overflow");
    }
}

impl Add for Span {
    type Output = Span;

    fn add(mut self, other: Span) -> Span {
        self += other;
        self
    }
}

impl AddAssign for Span {
    fn add_assign(&mut self, other: Span) {
        if self.is_empty() {
            *self = other;
        } else if !other.is_empty() {
            self.start = cmp::min(self.start, other.start);
            self.end = cmp::max(self.end, other.end);
        }
    }
}
