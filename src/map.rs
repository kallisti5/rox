// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use std::cmp;
use std::collections::hash_map::{Entry, RandomState};
use std::collections::HashMap;
use std::fmt::{Debug, Formatter, Result as FmtResult};
use std::hash::{BuildHasher, Hash};
use std::mem;
use std::slice::Iter as SliceIter;

// Keys in map point into key_vals, if an entry is removed from map,
// it must also be removed from key_vals.
pub struct OrderMap<K, V, S = RandomState>
where
    K: 'static + Eq + ?Sized,
{
    key_vals: Vec<(Box<K>, V)>,
    map: HashMap<&'static K, usize, S>,
}

impl<K, V, S> Clone for OrderMap<K, V, S>
where
    Box<K>: Clone,
    K: Eq + Hash + ?Sized,
    V: Clone,
    S: BuildHasher + Clone,
{
    fn clone(&self) -> OrderMap<K, V, S> {
        let mut ret = OrderMap::<K, V, S> {
            key_vals: Vec::new(),
            map: HashMap::with_hasher(self.map.hasher().clone()),
        };
        for (key, val) in self.key_vals.iter() {
            ret.insert(key.clone(), val.clone());
        }
        ret
    }

    fn clone_from(&mut self, src: &Self) {
        self.key_vals.clear();
        // cannot simply set hasher, so we have to create a new HashMap
        self.map = HashMap::with_hasher(src.map.hasher().clone());
        for (key, val) in src.key_vals.iter() {
            self.insert(key.clone(), val.clone());
        }
    }
}

impl<K, V, S> Debug for OrderMap<K, V, S>
where
    K: Debug + Eq + Hash + ?Sized,
    V: Debug,
    S: BuildHasher,
{
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        f.debug_map().entries(self.iter()).finish()
    }
}

impl<K, V, S> Default for OrderMap<K, V, S>
where
    K: Eq + Hash + ?Sized,
    S: BuildHasher + Default,
{
    fn default() -> OrderMap<K, V, S> {
        OrderMap::with_hasher(Default::default())
    }
}

impl<K, V> OrderMap<K, V, RandomState>
where
    K: Eq + Hash + ?Sized,
{
    pub fn new() -> OrderMap<K, V, RandomState> {
        OrderMap {
            key_vals: Vec::new(),
            map: HashMap::new(),
        }
    }

    pub fn with_capacity(capacity: usize) -> OrderMap<K, V, RandomState> {
        OrderMap {
            key_vals: Vec::with_capacity(capacity),
            map: HashMap::with_capacity(capacity),
        }
    }
}

impl<K, V, S> OrderMap<K, V, S>
where
    K: Eq + Hash + ?Sized,
    S: BuildHasher,
{
    pub fn with_hasher(hash_builder: S) -> OrderMap<K, V, S> {
        OrderMap {
            key_vals: Vec::new(),
            map: HashMap::with_hasher(hash_builder),
        }
    }

    pub fn with_capacity_and_hasher(
        capacity: usize,
        hash_builder: S,
    ) -> OrderMap<K, V, S> {
        OrderMap {
            key_vals: Vec::with_capacity(capacity),
            map: HashMap::with_capacity_and_hasher(capacity, hash_builder),
        }
    }

    pub fn hasher(&self) -> &S {
        self.map.hasher()
    }

    pub fn capacity(&self) -> usize {
        cmp::min(self.key_vals.capacity(), self.map.capacity())
    }

    pub fn reserve(&mut self, additional: usize) {
        self.key_vals.reserve(additional);
        self.map.reserve(additional);
    }

    pub fn shrink_to_fit(&mut self) {
        self.key_vals.shrink_to_fit();
        self.map.shrink_to_fit();
    }

    /// # Examples
    ///
    /// ```rust
    /// use rox::map::OrderMap;
    /// let mut map = OrderMap::<str, _>::new();
    /// map.insert("First".into(), 1);
    /// map.insert("Second".into(), 2);
    /// map.insert("Third".into(), 3);
    /// for (i, (key, val)) in map.iter().enumerate() {
    ///     match i {
    ///         0 => assert!(key == "First" && *val == 1),
    ///         1 => assert!(key == "Second" && *val == 2),
    ///         2 => assert!(key == "Third" && *val == 3),
    ///         _ => unreachable!(),
    ///     }
    /// }
    /// ```
    pub fn iter(&self) -> OrderMapIter<K, V> {
        OrderMapIter {
            iter: self.key_vals.iter(),
        }
    }

    pub fn len(&self) -> usize {
        self.key_vals.len()
    }

    pub fn is_empty(&self) -> bool {
        self.key_vals.is_empty()
    }

    pub fn clear(&mut self) {
        self.key_vals.clear();
        self.map.clear();
    }

    pub fn get(&self, k: &K) -> Option<&V> {
        self.map.get(k).map(|i| &self.key_vals[*i].1)
    }

    pub fn contains_key(&self, k: &K) -> bool {
        self.map.contains_key(k)
    }

    pub fn get_mut(&mut self, k: &K) -> Option<&mut V> {
        self.map
            .get(k)
            .cloned()
            .map(move |i| &mut self.key_vals[i].1)
    }

    /// # Examples
    ///
    /// ```rust
    /// use rox::map::OrderMap;
    /// let mut map = OrderMap::<str, _>::new();
    /// map.insert("First".into(), 1);
    /// map.insert("Second".into(), 2);
    /// map.insert("Third".into(), 3);
    /// let removed = map.swap_remove("First");
    /// assert_eq!(removed,Some(1));
    /// for (i, (key, val)) in map.iter().enumerate() {
    ///     match i {
    ///         0 => assert!(key == "Third" && *val == 3),
    ///         1 => assert!(key == "Second" && *val == 2),
    ///         _ => unreachable!(),
    ///     }
    /// }
    /// ```
    pub fn swap_remove(&mut self, k: &K) -> Option<V> {
        let index = self.map.remove(k)?;
        let v = self.key_vals.swap_remove(index).1;
        *self.map.get_mut(&*self.key_vals[index].0).unwrap() = index;
        Some(v)
    }

    pub fn insert(&mut self, k: Box<K>, v: V) -> Option<V> {
        // Use unsafe code to create a &'static K referring to key.
        // This is fine because:
        // * if the entry is vacant, key will be stored in key_vals
        //   and will not be dropped.
        // * if the entry is occupied, the created key_str will not be
        //   used and key can be safely dropped.
        let key_ref = unsafe { mem::transmute::<&K, &'static K>(&k) };
        match self.map.entry(key_ref) {
            Entry::Vacant(vacant) => {
                vacant.insert(self.key_vals.len());
                self.key_vals.push((k, v));
                None
            }
            Entry::Occupied(o) => {
                let vec_index = *o.get();
                Some(mem::replace(&mut self.key_vals[vec_index].1, v))
            }
        }
    }

    /// # Examples
    ///
    /// ```rust
    /// use rox::map::OrderMap;
    /// let mut map = OrderMap::<str, _>::new();
    /// map.insert("First".into(), 1);
    /// map.insert("Second".into(), 2);
    /// map.insert("Third".into(), 3);
    /// let removed = map.remove("First");
    /// assert_eq!(removed,Some(1));
    /// for (i, (key, val)) in map.iter().enumerate() {
    ///     match i {
    ///         0 => assert!(key == "Second" && *val == 2),
    ///         1 => assert!(key == "Third" && *val == 3),
    ///         _ => unreachable!(),
    ///     }
    /// }
    /// ```
    pub fn remove(&mut self, k: &K) -> Option<V> {
        let index = self.map.remove(k)?;
        let v = self.key_vals.remove(index).1;
        for i in self.map.values_mut().filter(|i| **i > index) {
            *i -= 1;
        }
        Some(v)
    }

    /// # Examples
    ///
    /// ```rust
    /// use rox::map::OrderMap;
    /// let mut map = OrderMap::<str, _>::new();
    /// map.insert("Hello".into(), 4);
    /// map.insert("World".into(), 10);
    /// map.retain(|_, val| *val > 5);
    /// assert_eq!(map.len(), 1);
    /// assert_eq!(map.get("Hello"), None);
    /// assert_eq!(map.get("World"), Some(&10));
    /// ```
    pub fn retain<F>(&mut self, mut f: F)
    where
        F: FnMut(&K, &V) -> bool,
    {
        let mut index = 0;
        let mut deleted = false;
        let map = &mut self.map;
        self.key_vals.retain(|key_val| {
            if f(&key_val.0, &key_val.1) {
                if deleted {
                    *map.get_mut(&*key_val.0).unwrap() = index;
                }
                index += 1;
                true
            } else {
                deleted = true;
                map.remove(&*key_val.0);
                false
            }
        });
    }
}

#[derive(Debug)]
pub struct OrderMapIter<'a, K, V>
where
    K: 'a + ?Sized,
    V: 'a,
{
    iter: SliceIter<'a, (Box<K>, V)>,
}

impl<'a, K, V> Default for OrderMapIter<'a, K, V>
where
    K: 'a + ?Sized,
    V: 'a,
{
    fn default() -> Self {
        OrderMapIter { iter: [].iter() }
    }
}

impl<'a, K, V> Clone for OrderMapIter<'a, K, V>
where
    K: ?Sized,
{
    fn clone(&self) -> OrderMapIter<'a, K, V> {
        OrderMapIter {
            iter: self.iter.clone(),
        }
    }
}

impl<'a, K, V> Iterator for OrderMapIter<'a, K, V>
where
    K: ?Sized,
{
    type Item = (&'a K, &'a V);
    #[inline]
    fn next(&mut self) -> Option<(&'a K, &'a V)> {
        self.iter.next().map(|i| (&(i.0) as &K, &(i.1)))
    }
}
