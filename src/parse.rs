// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use error::Error;
use input::{SourceSet, Span};
use node::{self, Node};
use push_vec::{Index, PushVec};

use rug::float::Special;
use rug::{Assign, Float, Integer};
use std::cmp::{self, Ordering};

// Prcedence (high to low):
//
// ()
// . [] (left to right)
// unary ! + - & | ^ !& !| !^ (right to left)
// as (left to right)
// ** (left to right)
// * / % (left to right)
// + - (left to right)
// << >> (left to right)
// & | ^ !& !| !^ ~ (left to right, no mixing)
// == != < <= > >= (chaining)
// .. ..= +.. -.. (no chaining)
//
// : => -> = , ; @ (not in expressions)

#[derive(Clone, Copy, Debug, Hash, Eq, Ord, PartialEq, PartialOrd)]
pub enum TokenKind {
    Colon,       // :
    WideArrow,   // =>
    NarrowArrow, // ->
    Equals,      // =
    Comma,       // ,
    Semicolon,   // ;
    At,          // @

    To,        // ..
    ToInclude, // ..=
    PlusTo,    // +..
    MinusTo,   // -..

    IsEquals,      // ==
    NotEquals,     // !=
    Less,          // <
    LessEquals,    // <=
    Greater,       // >
    GreaterEquals, // >=

    Not,        // !
    And,        // &
    Or,         // |
    Xor,        // ^
    Nand,       // !&
    Nor,        // !|
    Xnor,       // !^
    Cat,        // ~
    ShiftLeft,  // <<
    ShiftRight, // >>

    Plus,      // +
    Minus,     // -
    Times,     // *
    Division,  // /
    Remainder, // %
    Power,     // **

    Point,       // .
    OpenParens,  // (
    CloseParens, // )
    OpenSquare,  // [
    CloseSquare, // ]
    OpenBrace,   // {
    CloseBrace,  // }

    KeywordArray,
    KeywordAs,
    KeywordComp,
    KeywordElse,
    KeywordIf,
    KeywordIn,
    KeywordMatch,
    KeywordOut,
    KeywordVariant,

    Integer,
    IntegerXz,
    Float,
    FloatI,
    ZeroX,
    ZeroZ,
    Word,
    Eof,
    Unknown,
}

#[derive(Clone, Copy, Debug, Hash, Eq, Ord, PartialEq, PartialOrd)]
struct Token {
    kind: TokenKind,
    span: Span,
}

fn get_keyword(s: &str) -> Option<TokenKind> {
    let keyword = match s {
        "array" => TokenKind::KeywordArray,
        "as" => TokenKind::KeywordAs,
        "comp" => TokenKind::KeywordComp,
        "else" => TokenKind::KeywordElse,
        "if" => TokenKind::KeywordIf,
        "in" => TokenKind::KeywordIn,
        "match" => TokenKind::KeywordMatch,
        "out" => TokenKind::KeywordOut,
        "variant" => TokenKind::KeywordVariant,
        _ => return None,
    };
    Some(keyword)
}

// First character is known to be '_' | 'a'...'z' | 'A'...'Z'.
// Token kind is Keyword* or Word.
fn get_word_bytes(s: &str) -> (TokenKind, usize) {
    let mut iter = s.char_indices();

    let mut bytes = match iter.next() {
        Some((index, c))
            if c == '_' || ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') =>
        {
            index + c.len_utf8()
        }
        _ => unreachable!(),
    };
    for (index, c) in iter {
        if c == '_'
            || ('a' <= c && c <= 'z')
            || ('A' <= c && c <= 'Z')
            || ('0' <= c && c <= '9')
        {
            bytes = index + c.len_utf8();
        } else {
            break;
        }
    }
    let kind = get_keyword(&s[0..bytes]).unwrap_or(TokenKind::Word);
    (kind, bytes)
}

// First character is known to be '.' | '0'...'9'.
// Token kind is Point, ZeroZ, ZeroX, Integer or Float.
fn get_number_bytes(s: &str) -> Result<(TokenKind, usize), (String, usize)> {
    let mut base = 10;
    let mut has_point = false;
    let mut iter = s.char_indices();
    let (mut prev, mut bytes): (char, usize) = match iter.next() {
        Some((index0, c0 @ '.')) => {
            has_point = true;
            match iter.next() {
                Some((index1, c1 @ '0'...'9')) => (c1, index1 + c1.len_utf8()),
                _ => return Ok((TokenKind::Point, index0 + c0.len_utf8())),
            }
        }
        Some((index0, c0 @ '0')) => if let Some((index1, c1)) = iter.next() {
            match c1 {
                '0'...'9' | '_' => {}
                '.' => has_point = true,
                'b' => base = 2,
                'q' => base = 4,
                'o' => base = 8,
                'x' => base = 16,
                'z' => return Ok((TokenKind::ZeroZ, index1 + c1.len_utf8())),
                _ => return Ok((TokenKind::Integer, index0 + c0.len_utf8())),
            }
            (c1, index1 + c1.len_utf8())
        } else {
            return Ok((TokenKind::Integer, index0 + c0.len_utf8()));
        },
        Some((index0, c0 @ '1'...'9')) => (c0, index0 + c0.len_utf8()),
        _ => unreachable!(),
    };
    let mut has_digits = base == 10;
    let mut has_mode = false;
    let mut is_imag = false;
    let mut has_exponent = false;
    while let Some((index, c)) = iter.next() {
        let prev_bytes = bytes;
        bytes = index + c.len_utf8();
        match c {
            '_' if prev != '.' => {}
            '0'...'9' if c as i32 - '0' as i32 >= base => {
                let msg = format!("digit '{}' too large for base {}", c, base);
                Err((msg, bytes))?
            }
            '0'...'9' => has_digits = true,
            'a'...'f' | 'A'...'F' if base == 16 => has_digits = true,
            'x' | 'z' if base != 10 && !has_point => {
                has_digits = true;
                has_mode = true;
            }
            '.' if prev != '_' && !has_mode && !has_point => {
                has_point = true;
            }
            'e' | 'E' if base == 10 => {
                if prev == '_' {
                    let msg = "exponent cannot come directly after '_'";
                    Err((msg.into(), bytes))?
                }
                has_exponent = true;
                break;
            }
            'p' | 'P' if base != 10 && !has_mode => {
                if prev == '_' {
                    let msg = "exponent cannot come directly after '_'";
                    Err((msg.into(), bytes))?
                }
                if !has_digits {
                    let msg = "cannot have float literal with no digits";
                    Err((msg.into(), bytes))?
                }
                has_exponent = true;
                break;
            }
            'i' | 'I' if !has_mode => {
                is_imag = true;
                break;
            }
            'a'...'z' | 'A'...'Z' => {
                let msg =
                    format!("digit '{}' not allowed for base {}", c, base);
                Err((msg, bytes))?
            }
            _ => {
                bytes = prev_bytes;
                break;
            }
        }
        prev = c;
    }
    if has_exponent {
        let item = match iter.next() {
            Some((index, c @ '+')) | Some((index, c @ '-')) => {
                bytes = index + c.len_utf8();
                // Will never be used:
                // prev = c;
                iter.next()
            }
            item => item,
        };
        match item {
            Some((index, c @ '0'...'9')) => {
                prev = c;
                bytes = index + c.len_utf8();
            }
            Some((_, '_')) => {
                let msg = "exponent cannot start with '_'";
                Err((msg.into(), bytes))?
            }
            _ => {
                let msg = "exponent must have at least one decimal digit";
                Err((msg.into(), bytes))?
            }
        };
        while let Some((index, c)) = iter.next() {
            match c {
                '_' | '0'...'9' => {
                    prev = c;
                    bytes = index + c.len_utf8();
                }
                'i' | 'I' => {
                    is_imag = true;
                    bytes = index + c.len_utf8();
                    break;
                }
                'a'...'z' | 'A'...'Z' => {
                    let msg = format!("digit '{}' not allowed for exponent", c);
                    Err((msg, bytes))?
                }
                _ => break,
            }
        }
    }
    if is_imag {
        match iter.next().map(|(_, c)| c).unwrap_or('\0') {
            c @ 'a'...'z' | c @ 'A'...'Z' | c @ '_' | c @ '0'...'9' => {
                let msg = format!(
                    "character '{}' cannot follow imaginary number suffix",
                    c
                );
                Err((msg, bytes))
            }
            _ => Ok((TokenKind::FloatI, bytes)),
        }
    } else if prev == '_' {
        Err(("number literal cannot end in '_'".into(), bytes))
    } else if has_exponent || has_point {
        Ok((TokenKind::Float, bytes))
    } else if has_mode {
        Ok((TokenKind::IntegerXz, bytes))
    } else if has_digits {
        Ok((TokenKind::Integer, bytes))
    } else if base == 16 && prev == 'x' {
        // 0x is valid without digits, but cannot have trailing underscores
        Ok((TokenKind::ZeroX, bytes))
    } else {
        Err(("number literal must have at least one digit".into(), bytes))
    }
}

impl TokenKind {
    pub fn as_str(self) -> &'static str {
        use self::TokenKind::*;
        match self {
            WideArrow => "=>",
            NarrowArrow => "->",
            Equals => "=",
            Comma => ",",
            Semicolon => ";",
            Colon => ":",
            At => "@",
            To => "..",
            ToInclude => "..=",
            PlusTo => "+..",
            MinusTo => "-..",
            IsEquals => "==",
            NotEquals => "!=",
            Less => "<",
            LessEquals => "<=",
            Greater => ">",
            GreaterEquals => ">=",
            And => "&",
            Or => "|",
            Xor => "^",
            Nand => "!&",
            Nor => "!|",
            Xnor => "!^",
            Cat => "~",
            ShiftLeft => "<<",
            ShiftRight => ">>",
            Plus => "+",
            Minus => "-",
            Times => "*",
            Division => "/",
            Remainder => "%",
            Power => "**",
            Not => "!",
            Point => ".",
            OpenParens => "(",
            CloseParens => ")",
            OpenSquare => "[",
            CloseSquare => "]",
            OpenBrace => "{",
            CloseBrace => "}",
            KeywordArray => "array",
            KeywordAs => "as",
            KeywordComp => "comp",
            KeywordElse => "else",
            KeywordIf => "if",
            KeywordIn => "in",
            KeywordMatch => "match",
            KeywordOut => "out",
            KeywordVariant => "variant",
            Integer => "Integer",
            IntegerXz => "IntegerXz",
            Float => "Float",
            FloatI => "FloatI",
            ZeroX => "ZeroX",
            ZeroZ => "ZeroZ",
            Word => "Word",
            Eof => "Eof",
            Unknown => "Unknown",
        }
    }

    pub fn reverse_rel(self) -> TokenKind {
        use self::TokenKind::*;
        match self {
            Less => Greater,
            LessEquals => GreaterEquals,
            Greater => Less,
            GreaterEquals => LessEquals,
            _ => self,
        }
    }
}

impl Token {
    fn next(sources: &SourceSet, span: &mut Span) -> Result<Token, Error> {
        sources.skip_whitespace(span);
        let stream = sources.get(*span);
        let mut ret = Token {
            kind: TokenKind::Unknown,
            span: *span,
        };
        let mut iter = stream.char_indices();
        let (first_c, first_len) = match iter.next() {
            Some((index, c)) => (c, index + c.len_utf8()),
            None => {
                ret.kind = TokenKind::Eof;
                return Ok(ret);
            }
        };
        ret.span.set_len(first_len as u32);

        if let Some((index, second_c)) = iter.next() {
            if let Some((index, third_c)) = iter.next() {
                // check for three-character symbols
                ret.kind = match (first_c, second_c, third_c) {
                    ('.', '.', '=') => TokenKind::ToInclude,
                    ('+', '.', '.') => TokenKind::PlusTo,
                    ('-', '.', '.') => TokenKind::MinusTo,
                    _ => TokenKind::Unknown,
                };
                if ret.kind != TokenKind::Unknown {
                    ret.span.set_len((index + third_c.len_utf8()) as u32);
                    span.advance_start(ret.span.len());
                    return Ok(ret);
                }
            }

            // check for two-character symbols
            ret.kind = match (first_c, second_c) {
                ('=', '>') => TokenKind::WideArrow,
                ('-', '>') => TokenKind::NarrowArrow,
                ('=', '=') => TokenKind::IsEquals,
                ('!', '=') => TokenKind::NotEquals,
                ('<', '=') => TokenKind::LessEquals,
                ('>', '=') => TokenKind::GreaterEquals,
                ('!', '&') => TokenKind::Nand,
                ('!', '|') => TokenKind::Nor,
                ('!', '^') => TokenKind::Xnor,
                ('<', '<') => TokenKind::ShiftLeft,
                ('>', '>') => TokenKind::ShiftRight,
                ('*', '*') => TokenKind::Power,
                _ => TokenKind::Unknown,
            };
            if ret.kind != TokenKind::Unknown {
                ret.span.set_len((index + second_c.len_utf8()) as u32);
                span.advance_start(ret.span.len());
                return Ok(ret);
            }
        }

        ret.kind = match first_c {
            '=' => TokenKind::Equals,
            ',' => TokenKind::Comma,
            ';' => TokenKind::Semicolon,
            ':' => TokenKind::Colon,
            '@' => TokenKind::At,
            '<' => TokenKind::Less,
            '>' => TokenKind::Greater,
            '&' => TokenKind::And,
            '|' => TokenKind::Or,
            '^' => TokenKind::Xor,
            '~' => TokenKind::Cat,
            '+' => TokenKind::Plus,
            '-' => TokenKind::Minus,
            '*' => TokenKind::Times,
            '/' => TokenKind::Division,
            '%' => TokenKind::Remainder,
            '!' => TokenKind::Not,
            '(' => TokenKind::OpenParens,
            ')' => TokenKind::CloseParens,
            '[' => TokenKind::OpenSquare,
            ']' => TokenKind::CloseSquare,
            '{' => TokenKind::OpenBrace,
            '}' => TokenKind::CloseBrace,
            '0'...'9' | '.' => {
                let (kind, bytes) = match get_number_bytes(stream) {
                    Ok(x) => x,
                    Err((msg, bytes)) => {
                        let mut err_span = *span;
                        err_span.set_len(bytes as u32);
                        Err(sources.err(err_span, &msg))?
                    }
                };
                ret.span.set_len(bytes as u32);
                kind
            }
            '_' | 'a'...'z' | 'A'...'Z' => {
                let (kind, bytes) = get_word_bytes(stream);
                ret.span.set_len(bytes as u32);
                kind
            }
            _ => TokenKind::Unknown,
        };
        span.advance_start(ret.span.len());
        Ok(ret)
    }

    // This must not be called on anything except TokenKind::Integer
    // or TokenKind::IntegerXz.
    fn integer_literal(self, sources: &SourceSet) -> (Integer, Integer) {
        let mut buf = String::with_capacity(self.span.len() as usize);
        let mut radix = 10;
        let mut iter = sources.get(self.span).chars();
        match iter.next().expect("character") {
            '0' => match iter.next() {
                Some('_') => {}
                Some(c @ '0'...'9') => buf.push(c),
                Some('b') => radix = 2,
                Some('q') => radix = 4,
                Some('o') => radix = 8,
                Some('x') => radix = 16,
                Some(_) => unreachable!(),
                None => return (0.into(), 0.into()),
            },
            c @ '1'...'9' => buf.push(c),
            _ => unreachable!(),
        }
        let (max_digit_c, max_digit, digit_bits) = match radix {
            2 => ('1', 1, 1),
            4 => ('3', 3, 2),
            8 => ('7', 7, 3),
            16 => ('f', 15, 4),
            _ => ('\0', 0u32, 0u32),
        };
        let mut mode = Integer::new();
        for c in iter {
            let (cval, mode_digit) = match c {
                '_' => continue,
                '0'...'9' | 'a'...'f' | 'A'...'F' => (c, 0),
                'x' => ('0', max_digit),
                'z' => (max_digit_c, max_digit),
                _ => unreachable!(),
            };
            buf.push(cval);
            mode <<= digit_bits;
            mode |= mode_digit;
        }
        (buf.parse().expect("Integer"), mode)
    }

    // This must not be called on anything except TokenKind::Float or
    // TokenKind::FloatI.
    fn float_literal(self, sources: &SourceSet) -> Float {
        let mut buf = String::with_capacity(self.span.len() as usize);
        let mut radix = 10;
        let mut significant_digits = 0;
        let mut iter = sources.get(self.span).chars();
        let mut bin_exp = None;
        match iter.next().expect("character") {
            '.' => buf.push('.'),
            '0' => match iter.next().expect("character") {
                '_' => {}
                c @ '0' | c @ '.' => buf.push(c),
                c @ '1'...'9' => {
                    significant_digits = 1;
                    buf.push(c);
                }
                'b' => radix = 2,
                'q' => radix = 4,
                'o' => radix = 8,
                'x' => radix = 16,
                _ => unreachable!(),
            },
            c @ '1'...'9' => {
                significant_digits = 1;
                buf.push(c);
            }
            _ => unreachable!(),
        }
        while let Some(c) = iter.next() {
            match c {
                '_' => continue,
                '.' => {
                    buf.push(c);
                    continue;
                }
                '0'...'9' => {
                    buf.push(c);
                    if c != '0' || significant_digits != 0 {
                        significant_digits += 1;
                    }
                }
                'a'...'f' | 'A'...'F' if radix == 16 => {
                    buf.push(c);
                    significant_digits += 1;
                }
                'e' | 'E' if radix <= 10 => {
                    buf.push('@');
                    break;
                }
                'p' | 'P' => {
                    bin_exp = Some(buf.len());
                    break;
                }
                'i' | 'I' => {
                    break;
                }
                _ => unreachable!(),
            };
        }
        if let Some(c) = iter.next() {
            match c {
                '+' => {}
                '-' | '0'...'9' => buf.push(c),
                _ => unreachable!(),
            }
        }
        for c in iter {
            match c {
                '_' => {}
                '0'...'9' => buf.push(c),
                'i' | 'I' => break,
                _ => unreachable!(),
            }
        }
        let mut precision = match radix {
            2 => significant_digits,
            4 => significant_digits * 2,
            8 => significant_digits * 3,
            10 => (significant_digits * 10 + 2) / 3,
            16 => significant_digits * 4,
            _ => unreachable!(),
        };
        precision = cmp::max(precision, 53);
        if let Some(bin_exp) = bin_exp {
            let mant = &buf[0..bin_exp];
            let parse = Float::parse_radix(mant, radix).expect("Float");
            let mut f = Float::with_val(precision, parse);
            if !f.is_zero() {
                let exp = buf[bin_exp..].parse::<Integer>().expect("Integer");
                if let Some(exp) = exp.to_i32() {
                    f <<= exp;
                } else if exp.cmp0() == Ordering::Less {
                    f.assign(Special::Zero);
                } else {
                    f.assign(Special::Infinity);
                }
            }
            f
        } else {
            let parse = Float::parse_radix(&buf, radix).expect("Float");
            Float::with_val(precision, parse)
        }
    }
}

pub fn parse(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let full_span = *span;
    let mut stats = Vec::new();
    loop {
        sources.skip_whitespace(span);
        if span.is_empty() {
            break;
        }
        stats.push(parse_stat(sources, span, nodes)?);
    }
    let file = node::file(stats.into_boxed_slice(), nodes);
    nodes[file].set_span(full_span);
    Ok(file)
}

fn parse_stat(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let save_span = *span;
    let token = Token::next(sources, span)?;
    match token.kind {
        TokenKind::At => {
            return parse_event(token, sources, span, nodes);
        }
        TokenKind::Cat => {
            return parse_cat_assign(token, sources, span, nodes);
        }
        TokenKind::KeywordIf | TokenKind::KeywordMatch => {
            return parse_cond(token, sources, span, nodes);
        }
        TokenKind::OpenBrace => {
            return parse_block(Some(token), sources, span, nodes);
        }
        _ => *span = save_span,
    }
    let expr = parse_expr(sources, span, nodes)?;
    let after_expr = Token::next(sources, span)?;
    let (lhs, rhs, reset) = match after_expr.kind {
        TokenKind::Semicolon => {
            let stat = node::stat_expr(expr, nodes);
            nodes[stat].set_span(token.span + after_expr.span);
            return Ok(stat);
        }
        TokenKind::Equals => {
            if let Some(cat) = next_matches(sources, span, TokenKind::Cat) {
                return parse_cat(expr, cat, sources, span, nodes);
            }
            let rhs = parse_expr(sources, span, nodes)?;
            (expr, rhs, None)
        }
        TokenKind::WideArrow => {
            let lhs = parse_expr(sources, span, nodes)?;
            parse_token(sources, span, TokenKind::Equals, "")?;
            let rhs = parse_expr(sources, span, nodes)?;
            (lhs, rhs, Some(expr))
        }
        _ => Err(sources.err(after_expr.span, "expected '=', ';' or '=>'"))?,
    };
    let semicolon = parse_token(sources, span, TokenKind::Semicolon, "")?;
    let stat = node::stat_assign(lhs, rhs, reset, nodes);
    nodes[stat].set_span(token.span + semicolon.span);
    Ok(stat)
}

fn parse_block(
    open_token: Option<Token>,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let open_token = match open_token {
        Some(token) => token,
        None => parse_token(sources, span, TokenKind::OpenBrace, "")?,
    };
    let mut stats = Vec::new();
    let close = loop {
        if let Some(close) = next_matches(sources, span, TokenKind::CloseBrace)
        {
            break close;
        }
        let stat = parse_stat(sources, span, nodes)?;
        stats.push(stat);
    };
    let block = node::stat_block(stats.into_boxed_slice(), nodes);
    nodes[block].set_span(open_token.span + close.span);
    Ok(block)
}

fn parse_cat(
    lhs: Index,
    cat_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let token = Token::next(sources, span)?;
    let rhs = match token.kind {
        TokenKind::KeywordComp => node::cat_comp(nodes),
        TokenKind::KeywordVariant => node::cat_variant(nodes),
        _ => Err(sources.err(token.span, "expected 'comp' or 'variant'"))?,
    };
    nodes[rhs].set_span(cat_token.span + token.span);
    let semicolon = parse_token(sources, span, TokenKind::Semicolon, "")?;
    let assign = node::stat_assign(lhs, rhs, None, nodes);
    let assign_span = nodes[lhs].span() + semicolon.span;
    nodes[assign].set_span(assign_span);
    Ok(assign)
}

fn parse_cat_assign(
    cat_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let ident = parse_ident(sources, span, nodes, "")?;
    parse_token(sources, span, TokenKind::Equals, "")?;
    let token = Token::next(sources, span)?;
    let cat = match token.kind {
        TokenKind::KeywordComp => {
            parse_comp(ExprPat::Pat, token, sources, span, nodes)?
        }
        TokenKind::KeywordVariant => {
            parse_variant(token, sources, span, nodes)?
        }
        _ => Err(sources.err(token.span, "expected 'comp' or 'variant'"))?,
    };
    let semicolon = parse_token(sources, span, TokenKind::Semicolon, "")?;
    let stat = node::stat_cat_assign(ident, cat, nodes);
    nodes[stat].set_span(cat_token.span + semicolon.span);
    Ok(stat)
}

fn parse_event(
    at_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let event = parse_expr(sources, span, nodes)?;
    parse_token(sources, span, TokenKind::WideArrow, "")?;
    let lhs = parse_expr(sources, span, nodes)?;
    parse_token(sources, span, TokenKind::Equals, "")?;
    let rhs = parse_expr(sources, span, nodes)?;
    let semicolon = parse_token(sources, span, TokenKind::Semicolon, "")?;
    let stat = node::stat_event(event, lhs, rhs, nodes);
    nodes[stat].set_span(at_token.span + semicolon.span);
    Ok(stat)
}

fn parse_cond(
    mut token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let mut cond_span = token.span;
    let mut selectors = Vec::new();
    loop {
        match token.kind {
            TokenKind::KeywordIf => {
                let if_sel = parse_if(token, sources, span, nodes)?;
                selectors.push(if_sel);
                cond_span += nodes[if_sel].span();
            }
            TokenKind::KeywordMatch => {
                let match_sels = parse_match(token, sources, span, nodes)?;
                let last_match = *match_sels.last().expect("empty match_sels");
                selectors.extend(match_sels);
                cond_span += nodes[last_match].span();
            }
            _ => unreachable!(),
        };
        if next_matches(sources, span, TokenKind::KeywordElse).is_none() {
            break;
        }
        let save_span = *span;
        token = Token::next(sources, span)?;
        match token.kind {
            TokenKind::KeywordIf | TokenKind::KeywordMatch => {}
            TokenKind::OpenBrace => {
                let else_stat = parse_block(Some(token), sources, span, nodes)?;
                let else_span = nodes[else_stat].span();
                let else_selector = node::selector_else(else_stat, nodes);
                nodes[else_selector].set_span(else_span);
                selectors.push(else_selector);
                cond_span += else_span;
                break;
            }
            _ => {
                *span = save_span;
                Err(sources.err(token.span, "expected 'if', 'match' or '{'"))?
            }
        }
    }
    let stat = node::stat_cond(selectors.into_boxed_slice(), nodes);
    nodes[stat].set_span(cond_span);
    Ok(stat)
}

fn parse_if(
    if_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let condition = parse_expr(sources, span, nodes)?;
    let stat = parse_block(None, sources, span, nodes)?;
    let stat_span = nodes[stat].span();
    let selector = node::selector_if(condition, stat, nodes);
    nodes[selector].set_span(if_token.span + stat_span);
    Ok(selector)
}

fn parse_match(
    match_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Vec<Index>, Error> {
    let select = parse_expr(sources, span, nodes)?;
    parse_token(sources, span, TokenKind::Colon, "")?;
    let mut selectors = Vec::new();
    let mut selector_span = match_token.span;
    loop {
        let mut patterns = Vec::new();
        let mut token;
        loop {
            patterns.push(parse_pattern(sources, span, nodes)?);
            token = Token::next(sources, span)?;
            match token.kind {
                TokenKind::Comma => {
                    if let Some(t) =
                        next_matches(sources, span, TokenKind::OpenBrace)
                    {
                        token = t;
                        break;
                    }
                }
                TokenKind::OpenBrace => break,
                _ => Err(sources.err(token.span, "expected '{' or ','"))?,
            }
        }
        let stat = parse_block(Some(token), sources, span, nodes)?;
        let stat_span = nodes[stat].span();
        let selector = node::selector_match(
            select,
            patterns.into_boxed_slice(),
            stat,
            nodes,
        );
        nodes[selector].set_span(selector_span + stat_span);
        selectors.push(selector);

        let before_else = *span;
        if let Some(else_token) =
            next_matches(sources, span, TokenKind::KeywordElse)
        {
            selector_span = else_token.span;
            match Token::next(sources, &mut span.clone())?.kind {
                // this match is finished
                TokenKind::KeywordIf
                | TokenKind::KeywordMatch
                | TokenKind::OpenBrace => *span = before_else,
                // new pattern list
                _ => continue,
            }
        }
        break;
    }
    Ok(selectors)
}

fn parse_pattern(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let token = Token::next(sources, span)?;
    match token.kind {
        TokenKind::WideArrow => {
            let ident = parse_ident(sources, span, nodes, "")?;
            let pat = node::pat_assign(ident, nodes);
            let pat_span = nodes[ident].span();
            nodes[pat].set_span(pat_span);
            Ok(pat)
        }
        TokenKind::IsEquals => {
            let expr = parse_expr(sources, span, nodes)?;
            let pat = node::pat_expr(expr, nodes);
            let pat_span = nodes[expr].span();
            nodes[pat].set_span(pat_span);
            Ok(pat)
        }
        TokenKind::Word => {
            let ident = node::ident(sources.get(token.span).into(), nodes);
            nodes[ident].set_span(token.span);
            let mut tag_span = token.span;
            let pat = if let Some(token) =
                next_matches(sources, span, TokenKind::OpenParens)
            {
                let pat = if is_record(sources, *span) {
                    parse_record(ExprPat::Pat, token, sources, span, nodes)?
                } else {
                    let (pat, close_span) =
                        parse_tuple(ExprPat::Pat, token, sources, span, nodes)?;
                    tag_span += close_span;
                    pat
                };
                tag_span += nodes[pat].span();
                pat
            } else {
                node::pat_tuple(Box::new([]), nodes)
            };
            let tag = node::pat_tag(ident, pat, nodes);
            nodes[tag].set_span(tag_span);
            Ok(tag)
        }
        TokenKind::ZeroX
        | TokenKind::ZeroZ
        | TokenKind::Integer
        | TokenKind::IntegerXz => {
            let expr = node_integer_literal(token, sources, nodes);
            let i = node::pat_expr(expr, nodes);
            let expr_span = nodes[expr].span();
            nodes[i].set_span(expr_span);
            Ok(i)
        }
        TokenKind::Float | TokenKind::FloatI => {
            let expr = node_float_literal(token, sources, nodes);
            let i = node::pat_expr(expr, nodes);
            let expr_span = nodes[expr].span();
            nodes[i].set_span(expr_span);
            Ok(i)
        }
        TokenKind::OpenParens => if is_record(sources, *span) {
            parse_record(ExprPat::Pat, token, sources, span, nodes)
        } else {
            parse_tuple(ExprPat::Pat, token, sources, span, nodes).map(|t| t.0)
        },
        _ => Err(sources.err(
            token.span,
            "expected '=>', '==', '(', tag or number literal",
        )),
    }
}

fn parse_comp(
    expr_pat: ExprPat,
    comp_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let input = match expr_pat {
        ExprPat::Expr => parse_expr(sources, span, nodes)?,
        ExprPat::Pat => parse_pattern(sources, span, nodes)?,
    };
    parse_token(sources, span, TokenKind::NarrowArrow, "")?;
    let output = parse_expr(sources, span, nodes)?;
    let body = parse_block(None, sources, span, nodes)?;
    let comp = node::component(input, output, body, nodes);
    let comp_span = comp_token.span + nodes[body].span();
    nodes[comp].set_span(comp_span);
    Ok(comp)
}

fn parse_variant(
    variant_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    parse_token(sources, span, TokenKind::OpenParens, "")?;
    let mut tags = Vec::new();
    let mut need_comma = false;
    let mut prev_skipped_brackets = false;
    let close = loop {
        if let Some(close) = next_matches(sources, span, TokenKind::CloseParens)
        {
            break close;
        }
        if need_comma {
            let expected = if prev_skipped_brackets {
                "expected '(', ')' or ','"
            } else {
                "expected ')' or ','"
            };
            parse_token(sources, span, TokenKind::Comma, expected)?;
            need_comma = false;
            continue;
        }
        let ident = parse_ident(sources, span, nodes, "expected ')' or tag")?;
        let mut tag_span = nodes[ident].span();
        let ty = if let Some(token) =
            next_matches(sources, span, TokenKind::OpenParens)
        {
            prev_skipped_brackets = false;
            let ty = if is_record(sources, *span) {
                parse_record(ExprPat::Expr, token, sources, span, nodes)?
            } else {
                let (ty, close_span) =
                    parse_tuple(ExprPat::Expr, token, sources, span, nodes)?;
                tag_span += close_span;
                ty
            };
            tag_span += nodes[ty].span();
            ty
        } else {
            prev_skipped_brackets = true;
            node::tuple(Box::new([]), nodes)
        };
        let tag = node::tag(ident, ty, nodes);
        nodes[tag].set_span(tag_span);
        tags.push(tag);
        need_comma = true;
    };
    let variant = node::variant(tags.into_boxed_slice(), nodes);
    nodes[variant].set_span(variant_token.span + close.span);
    Ok(variant)
}

fn parse_array(
    array_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    parse_token(sources, span, TokenKind::OpenParens, "")?;
    let ty = parse_expr(sources, span, nodes)?;
    let close = parse_token(sources, span, TokenKind::CloseParens, "")?;
    let array = node::array(ty, nodes);
    nodes[array].set_span(array_token.span + close.span);
    Ok(array)
}

// also returns span of closing bracket
fn parse_tuple(
    expr_pat: ExprPat,
    open_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<(Index, Span), Error> {
    let mut has_comma = false;
    let mut elems = Vec::new();
    let mut need_comma = false;
    let close = loop {
        if let Some(c) = next_matches(sources, span, TokenKind::CloseParens) {
            break c;
        }
        if need_comma {
            let expected = "expected ')' or ','";
            parse_token(sources, span, TokenKind::Comma, expected)?;
            has_comma = true;
            need_comma = false;
            continue;
        }
        let elem = match expr_pat {
            ExprPat::Expr => parse_expr(sources, span, nodes)?,
            ExprPat::Pat => parse_pattern(sources, span, nodes)?,
        };
        elems.push(elem);
        need_comma = true;
    };
    if !has_comma && !elems.is_empty() {
        return Ok((elems[0], close.span));
    }
    let tuple = match expr_pat {
        ExprPat::Expr => node::tuple(elems.into_boxed_slice(), nodes),
        ExprPat::Pat => node::pat_tuple(elems.into_boxed_slice(), nodes),
    };
    nodes[tuple].set_span(open_token.span + close.span);
    Ok((tuple, close.span))
}

fn parse_record(
    expr_pat: ExprPat,
    open_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let mut elems = Vec::new();
    let mut need_comma = false;
    let close = loop {
        if let Some(c) = next_matches(sources, span, TokenKind::CloseParens) {
            break c;
        }
        if need_comma {
            let expected = "expected ')' or ','";
            parse_token(sources, span, TokenKind::Comma, expected)?;
            need_comma = false;
            continue;
        }
        let expected = "expected ')' or field name";
        let ident = parse_ident(sources, span, nodes, expected)?;
        parse_token(sources, span, TokenKind::Colon, "")?;
        let elem = match expr_pat {
            ExprPat::Expr => parse_expr(sources, span, nodes)?,
            ExprPat::Pat => parse_pattern(sources, span, nodes)?,
        };
        let field = match expr_pat {
            ExprPat::Expr => node::field(ident, elem, nodes),
            ExprPat::Pat => node::pat_field(ident, elem, nodes),
        };
        elems.push(field);
        let field_span = nodes[ident].span() + nodes[elem].span();
        nodes[field].set_span(field_span);
        need_comma = true;
    };
    let record = match expr_pat {
        ExprPat::Expr => node::record(elems.into_boxed_slice(), nodes),
        ExprPat::Pat => node::pat_record(elems.into_boxed_slice(), nodes),
    };
    nodes[record].set_span(open_token.span + close.span);
    Ok(record)
}

fn parse_expr(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    parse_expr_range(sources, span, nodes)
}

fn parse_expr_range(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let lhs = parse_expr_relational(sources, span, nodes)?;
    let mut expr_span = nodes[lhs].span();
    let mut peek = *span;
    let token = Token::next(sources, &mut peek)?;
    match token.kind {
        TokenKind::To
        | TokenKind::ToInclude
        | TokenKind::PlusTo
        | TokenKind::MinusTo => *span = peek,
        _ => return Ok(lhs),
    }
    expr_span += token.span;
    let prev_span = *span;
    let rhs = match (token.kind, parse_expr_relational(sources, span, nodes)) {
        (_, Ok(i)) => {
            expr_span += nodes[i].span();
            Some(i)
        }
        (TokenKind::PlusTo, Err(e)) | (TokenKind::MinusTo, Err(e)) => Err(e)?,
        (_, Err(_)) => {
            *span = prev_span;
            None
        }
    };
    match Token::next(sources, &mut span.clone())?.kind {
        TokenKind::To
        | TokenKind::ToInclude
        | TokenKind::PlusTo
        | TokenKind::MinusTo => {
            Err(sources.err(token.span, "cannot have chained range operators"))?
        }
        _ => {}
    }
    let expr = match (token.kind, rhs) {
        (TokenKind::To, opt_rhs) => {
            node::expr_range(false, lhs, opt_rhs, nodes)
        }
        (TokenKind::ToInclude, opt_rhs) => {
            node::expr_range(true, lhs, opt_rhs, nodes)
        }
        (TokenKind::PlusTo, Some(rhs)) => {
            node::expr_range_sized(false, lhs, rhs, nodes)
        }
        (TokenKind::MinusTo, Some(rhs)) => {
            node::expr_range_sized(true, lhs, rhs, nodes)
        }
        _ => unreachable!(),
    };
    nodes[expr].set_span(expr_span);
    Ok(expr)
}

fn parse_expr_relational(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let lhs = parse_expr_bitwise_cat(sources, span, nodes)?;
    let lhs_span = nodes[lhs].span();
    let mut peek = *span;
    let mut token = Token::next(sources, &mut peek)?;
    match token.kind {
        TokenKind::IsEquals
        | TokenKind::NotEquals
        | TokenKind::Less
        | TokenKind::LessEquals
        | TokenKind::Greater
        | TokenKind::GreaterEquals => *span = peek,
        _ => return Ok(lhs),
    }
    let mut rhs = parse_expr_bitwise_cat(sources, span, nodes)?;
    let mut rhs_span = nodes[rhs].span();
    let rel = node::expr_rel(token.kind, lhs, rhs, nodes);
    let rel_span = lhs_span + rhs_span;
    nodes[rel].set_span(rel_span);

    let (mut top, mut top_span) = (rel, rel_span);
    loop {
        peek = *span;
        token = Token::next(sources, &mut peek)?;
        match token.kind {
            TokenKind::IsEquals
            | TokenKind::NotEquals
            | TokenKind::Less
            | TokenKind::LessEquals
            | TokenKind::Greater
            | TokenKind::GreaterEquals => *span = peek,
            _ => return Ok(top),
        }
        let (lhs, lhs_span) = (rhs, rhs_span);
        rhs = parse_expr_bitwise_cat(sources, span, nodes)?;
        rhs_span = nodes[rhs].span();
        let rel = node::expr_rel(token.kind, lhs, rhs, nodes);
        let rel_span = lhs_span + rhs_span;
        nodes[rel].set_span(rel_span);

        let and = node::expr_bitwise(TokenKind::And, top, rel, nodes);
        let and_span = top_span + rel_span;
        nodes[and].set_span(and_span);
        top = and;
        top_span = and_span;
    }
}

fn parse_expr_bitwise_cat(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let mut first: Option<TokenKind> = None;
    let mut lhs = parse_expr_shift(sources, span, nodes)?;
    loop {
        let mut peek = *span;
        let token = Token::next(sources, &mut peek)?;
        match token.kind {
            TokenKind::And
            | TokenKind::Or
            | TokenKind::Xor
            | TokenKind::Nand
            | TokenKind::Nor
            | TokenKind::Xnor
            | TokenKind::Cat => *span = peek,
            _ => return Ok(lhs),
        }
        let mixage = match (first, token.kind) {
            (None, _) => {
                first = Some(token.kind);
                None
            }
            (Some(first), second) if first == second => None,
            (Some(TokenKind::Cat), _) | (_, TokenKind::Cat) => {
                Some("bitwise with concatenation operators")
            }
            _ => Some("different bitwise operators"),
        };
        if let Some(mixage) = mixage {
            Err(sources.err(
                token.span,
                &format!("cannot mix {}, consider using brackets", mixage),
            ))?
        }
        let rhs = parse_expr_shift(sources, span, nodes)?;
        let expr = match token.kind {
            TokenKind::Cat => node::expr_concat(lhs, rhs, nodes),
            _ => node::expr_bitwise(token.kind, lhs, rhs, nodes),
        };
        let expr_span = nodes[lhs].span() + nodes[rhs].span();
        nodes[expr].set_span(expr_span);
        lhs = expr;
    }
}

fn parse_expr_shift(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let mut lhs = parse_expr_add(sources, span, nodes)?;
    loop {
        let mut peek = *span;
        let token = Token::next(sources, &mut peek)?;
        match token.kind {
            TokenKind::ShiftLeft | TokenKind::ShiftRight => *span = peek,
            _ => return Ok(lhs),
        }
        let rhs = parse_expr_add(sources, span, nodes)?;
        let shift = node::expr_shift(token.kind, lhs, rhs, nodes);
        let shift_span = nodes[lhs].span() + nodes[rhs].span();
        nodes[shift].set_span(shift_span);
        lhs = shift;
    }
}

fn parse_expr_add(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let mut lhs = parse_expr_mul(sources, span, nodes)?;
    loop {
        let mut peek = *span;
        let token = Token::next(sources, &mut peek)?;
        match token.kind {
            TokenKind::Plus | TokenKind::Minus => *span = peek,
            _ => return Ok(lhs),
        };
        let rhs = parse_expr_mul(sources, span, nodes)?;
        let add = node::expr_add(token.kind, lhs, rhs, nodes);
        let add_span = nodes[lhs].span() + nodes[rhs].span();
        nodes[add].set_span(add_span);
        lhs = add;
    }
}

fn parse_expr_mul(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let mut lhs = parse_expr_pow(sources, span, nodes)?;
    loop {
        let mut peek = *span;
        let token = Token::next(sources, &mut peek)?;
        match token.kind {
            TokenKind::Times | TokenKind::Division | TokenKind::Remainder => {
                *span = peek
            }
            _ => return Ok(lhs),
        }
        let rhs = parse_expr_pow(sources, span, nodes)?;
        let mul = node::expr_mul(token.kind, lhs, rhs, nodes);
        let mul_span = nodes[lhs].span() + nodes[rhs].span();
        nodes[mul].set_span(mul_span);
        lhs = mul;
    }
}

fn parse_expr_pow(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let mut lhs = parse_expr_cast(sources, span, nodes)?;
    loop {
        if next_matches(sources, span, TokenKind::Power).is_none() {
            return Ok(lhs);
        }
        let rhs = parse_expr_cast(sources, span, nodes)?;
        let pow = node::expr_pow(lhs, rhs, nodes);
        let pow_span = nodes[lhs].span() + nodes[rhs].span();
        nodes[pow].set_span(pow_span);
        lhs = pow;
    }
}

fn parse_expr_cast(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let mut lhs = parse_expr_unary(sources, span, nodes)?;
    loop {
        if next_matches(sources, span, TokenKind::KeywordAs).is_none() {
            return Ok(lhs);
        }
        let rhs = parse_expr_unary(sources, span, nodes)?;
        let cast = node::expr_cast(lhs, rhs, nodes);
        let cast_span = nodes[lhs].span() + nodes[rhs].span();
        nodes[cast].set_span(cast_span);
        lhs = cast;
    }
}

fn parse_expr_unary(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let mut peek = *span;
    let token = Token::next(sources, &mut peek)?;
    match token.kind {
        TokenKind::Not
        | TokenKind::And
        | TokenKind::Or
        | TokenKind::Xor
        | TokenKind::Nand
        | TokenKind::Nor
        | TokenKind::Xnor
        | TokenKind::Plus
        | TokenKind::Minus => {
            *span = peek;
        }
        _ => return parse_expr_brackets(sources, span, nodes),
    }
    let rhs = parse_expr_unary(sources, span, nodes)?;
    let unary = node::expr_unary(token.kind, rhs, nodes);
    let unary_span = token.span + nodes[rhs].span();
    nodes[unary].set_span(unary_span);
    Ok(unary)
}

fn parse_expr_brackets(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let atom = match next_matches(sources, span, TokenKind::OpenParens) {
        Some(token) => if is_record(sources, *span) {
            parse_record(ExprPat::Expr, token, sources, span, nodes)?
        } else {
            parse_tuple(ExprPat::Expr, token, sources, span, nodes)?.0
        },
        None => parse_expr_atom(sources, span, nodes)?,
    };
    parse_accessing(sources, span, nodes, atom)
}

// called after open parens is already parsed
fn is_record(sources: &SourceSet, span: Span) -> bool {
    let mut peek = span;
    next_matches(sources, &mut peek, TokenKind::Word).is_some()
        && next_matches(sources, &mut peek, TokenKind::Colon).is_some()
}

fn node_integer_literal(
    token: Token,
    sources: &SourceSet,
    nodes: &mut PushVec<Node>,
) -> Index {
    let (val, mode) = match token.kind {
        TokenKind::ZeroX => (0.into(), (-1).into()),
        TokenKind::ZeroZ => ((-1).into(), (-1).into()),
        TokenKind::Integer | TokenKind::IntegerXz => {
            token.integer_literal(sources)
        }
        _ => unreachable!(),
    };
    let i = if mode.cmp0() == Ordering::Equal {
        node::integer(val, nodes)
    } else {
        node::integer_xz(val, mode, nodes)
    };
    nodes[i].set_span(token.span);
    i
}

fn node_float_literal(
    token: Token,
    sources: &SourceSet,
    nodes: &mut PushVec<Node>,
) -> Index {
    let val = token.float_literal(sources);
    let i = if token.kind == TokenKind::Float {
        node::real(val, nodes)
    } else {
        node::imaginary(val, nodes)
    };
    nodes[i].set_span(token.span);
    i
}

fn parse_expr_atom(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let token = Token::next(sources, span)?;
    match token.kind {
        TokenKind::Word => {
            let ident = node::ident(sources.get(token.span).into(), nodes);
            nodes[ident].set_span(token.span);
            Ok(ident)
        }
        TokenKind::KeywordIn => {
            let input = node::input(nodes);
            nodes[input].set_span(token.span);
            Ok(input)
        }
        TokenKind::KeywordOut => {
            let output = node::output(nodes);
            nodes[output].set_span(token.span);
            Ok(output)
        }
        TokenKind::KeywordComp => {
            parse_comp(ExprPat::Expr, token, sources, span, nodes)
        }
        TokenKind::KeywordVariant => parse_variant(token, sources, span, nodes),
        TokenKind::KeywordArray => parse_array(token, sources, span, nodes),
        TokenKind::ZeroX
        | TokenKind::ZeroZ
        | TokenKind::Integer
        | TokenKind::IntegerXz => {
            Ok(node_integer_literal(token, sources, nodes))
        }
        TokenKind::Float | TokenKind::FloatI => {
            Ok(node_float_literal(token, sources, nodes))
        }
        _ => Err(sources.err(token.span, "expected expression")),
    }
}

fn parse_expr_field(
    expr: Index,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let field = parse_ident(sources, span, nodes, "expected field identifier")?;
    let access = node::expr_field(expr, field, nodes);
    let access_span = nodes[expr].span() + nodes[field].span();
    nodes[access].set_span(access_span);
    Ok(access)
}

fn parse_expr_param(
    expr: Index,
    open_token: Token,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let mut total_span = nodes[expr].span();
    let param = if is_record(sources, *span) {
        parse_record(ExprPat::Expr, open_token, sources, span, nodes)?
    } else {
        let (param, close_span) =
            parse_tuple(ExprPat::Expr, open_token, sources, span, nodes)?;
        total_span += close_span;
        param
    };
    total_span += nodes[param].span();

    let i = node::expr_param(expr, param, nodes);
    nodes[i].set_span(total_span);
    Ok(i)
}

fn parse_expr_index(
    expr: Index,
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let range = parse_expr(sources, span, nodes)?;
    let close = parse_token(sources, span, TokenKind::CloseSquare, "")?;
    let access = node::expr_index(expr, range, nodes);
    let access_span = nodes[expr].span() + close.span;
    nodes[access].set_span(access_span);
    Ok(access)
}

// Ranges on same lines are equivalent:
//
// 14 ..= 12    14 .. 13    14 +.. -1    12 -.. -1
// 14 ..= 13    14 .. 14    14 +.. 0     13 -.. 0
// 14 ..= 14    14 .. 15    14 +.. 1     14 -.. 1
// 14 ..= 15    14 .. 16    14 +.. 2     15 -.. 2
// 14 ..= 16    14 .. 17    14 +.. 3     16 -.. 3
//
// 15 ..= 13    15 .. 14    15 +.. -1    13 -.. -1
// 14 ..= 13    14 .. 14    14 +.. 0     13 -.. 0
// 13 ..= 13    13 .. 14    13 +.. 1     13 -.. 1
// 12 ..= 13    12 .. 14    12 +.. 2     13 -.. 2
// 11 ..= 13    11 .. 14    11 +.. 3     13 -.. 3

fn parse_accessing(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
    mut expr: Index,
) -> Result<Index, Error> {
    loop {
        let save_span = *span;
        let token = Token::next(sources, span)?;
        match token.kind {
            TokenKind::Point => {
                expr = parse_expr_field(expr, sources, span, nodes)?;
            }
            TokenKind::OpenParens => {
                expr = parse_expr_param(expr, token, sources, span, nodes)?;
            }
            TokenKind::OpenSquare => {
                expr = parse_expr_index(expr, sources, span, nodes)?;
            }
            _ => {
                *span = save_span;
                return Ok(expr);
            }
        }
    }
}

fn parse_ident(
    sources: &SourceSet,
    span: &mut Span,
    nodes: &mut PushVec<Node>,
    expected: &str,
) -> Result<Index, Error> {
    let expected = if expected.is_empty() {
        "expected identifier"
    } else {
        expected
    };
    let token = parse_token(sources, span, TokenKind::Word, expected)?;
    let string = sources.get(token.span).into();
    let ident = node::ident(string, nodes);
    nodes[ident].set_span(token.span);
    Ok(ident)
}

fn parse_token(
    sources: &SourceSet,
    span: &mut Span,
    required: TokenKind,
    expected: &str,
) -> Result<Token, Error> {
    let token = Token::next(sources, span)?;
    match token.kind {
        x if x == required => Ok(token),
        _ => if expected.is_empty() {
            let expected = format!("expected '{}'", required.as_str());
            Err(sources.err(token.span, &expected))
        } else {
            Err(sources.err(token.span, expected))
        },
    }
}

fn next_matches(
    sources: &SourceSet,
    span: &mut Span,
    required: TokenKind,
) -> Option<Token> {
    let mut peek = *span;
    match parse_token(sources, &mut peek, required, "") {
        Ok(token) => {
            *span = peek;
            Some(token)
        }
        Err(_) => None,
    }
}

#[derive(Clone, Copy, Debug, Hash, Eq, Ord, PartialEq, PartialOrd)]
enum ExprPat {
    Expr,
    Pat,
}
