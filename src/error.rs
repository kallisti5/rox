// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use std::error;
use std::fmt;
use std::io;

#[derive(Debug)]
pub enum Error {
    Parse { message: String },
    Io { message: String, cause: io::Error },
    General { message: String },
}

impl Error {
    pub fn parse(message: &str) -> Error {
        Error::Parse {
            message: message.into(),
        }
    }

    pub fn io(filename: &str, cause: io::Error) -> Error {
        Error::Io {
            message: {
                use std::error::Error;
                format!("{}: {}", filename, cause.description())
            },
            cause,
        }
    }

    pub fn general(message: &str) -> Error {
        Error::General {
            message: message.into(),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Parse { message }
            | Error::Io { message, .. }
            | Error::General { message } => write!(f, "{}", message),
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match self {
            Error::Parse { message }
            | Error::Io { message, .. }
            | Error::General { message } => message,
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match self {
            Error::Io { cause, .. } => Some(cause),
            _ => None,
        }
    }
}
