// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use error::Error;
use input::SourceSet;
use netlist::Scope;
use node::{self, Node};
use push_vec::{Index, OptionIndex, PushVec};

use rug::Integer;
use std::fmt::{Debug, Formatter, Result as FmtResult};
use std::mem;

enum AssignGroup {
    One(AssignElem),
    Many(PushVec<AssignElem>),
}

impl AssignGroup {
    fn push(&mut self, elem: AssignElem) {
        match self {
            AssignGroup::One(_) => {
                match mem::replace(self, AssignGroup::Many(PushVec::new())) {
                    AssignGroup::One(one) => match self {
                        AssignGroup::Many(many) => {
                            many.push(one);
                            many.push(elem);
                        }
                        _ => unreachable!(),
                    },
                    _ => unreachable!(),
                }
            }
            AssignGroup::Many(many) => {
                many.push(elem);
            }
        }
    }
}

impl Debug for AssignGroup {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            AssignGroup::One(one) => f.debug_list().entry(one).finish(),
            AssignGroup::Many(many) => Debug::fmt(many, f),
        }
    }
}

#[derive(Debug)]
enum AssignElem {
    Assign(Assign),
    Selector { selector: Index, assign: Assign },
}

#[derive(Debug)]
enum Assign {
    Wire { reset: OptionIndex, val: Index },
    Reg { event: Index, val: Index },
}

#[derive(Debug)]
struct Dir {
    scopes: PushVec<Scope>,
    assigns: PushVec<AssignGroup>,
    node_scope: PushVec<OptionIndex>,
    node_checked: Integer,
}

fn create_dir(
    sources: &SourceSet,
    nodes: &PushVec<Node>,
    root: Index,
) -> Result<Dir, Error> {
    let mut dir = Dir {
        scopes: PushVec::new(),
        assigns: PushVec::new(),
        node_scope: PushVec::from_elem(None, nodes.len()),
        node_checked: Integer::new(),
    };
    grow_dir((sources, nodes), None, BlockKind::Normal, root, &mut dir)?;
    Ok(dir)
}

#[derive(Clone, Copy)]
enum BlockKind {
    Normal,
    Selector(Index),
    SelectorBlock(Index),
}

fn grow_dir(
    (sources, nodes): (&SourceSet, &PushVec<Node>),
    outer_scope: Option<Index>,
    block_kind: BlockKind,
    index: Index,
    dir: &mut Dir,
) -> Result<(), Error> {
    if dir.node_checked.get_bit(index.as_u32()) {
        return Ok(());
    }
    dir.node_checked.set_bit(index.as_u32(), true);

    if let Some(os) = outer_scope {
        check_assign((sources, nodes), os, block_kind, index, dir)?;
    }

    let scope = if nodes[index].has_scope() {
        dir.scopes.push(Scope::new(outer_scope))
    } else {
        outer_scope.expect("top dir node has no scope")
    };
    dir.node_scope[index] = scope.into();

    let block_kind = if nodes[index].get_selector().is_some() {
        BlockKind::Selector(index)
    } else if nodes[index].get_stat_block().is_some() {
        match block_kind {
            BlockKind::Selector(i) => BlockKind::SelectorBlock(i),
            _ => BlockKind::Normal,
        }
    } else {
        block_kind
    };

    for child in nodes[index].children() {
        let child = match child.as_option() {
            Some(s) => s,
            None => continue,
        };

        grow_dir((sources, nodes), Some(scope), block_kind, child, dir)?;
    }
    Ok(())
}

fn check_assign(
    (sources, nodes): (&SourceSet, &PushVec<Node>),
    scope: Index,
    kind: BlockKind,
    index: Index,
    dir: &mut Dir,
) -> Result<(), Error> {
    let node = &nodes[index];
    if let Some((reset, dst, val)) = node.get_stat_assign() {
        let reset = reset.into();
        let elem = Assign::Wire { reset, val };
        return add_assign_elem((sources, nodes), scope, kind, dst, elem, dir);
    }
    if let Some((event, dst, val)) = node.get_stat_event() {
        let elem = Assign::Reg { event, val };
        return add_assign_elem((sources, nodes), scope, kind, dst, elem, dir);
    }
    Ok(())
}

fn add_assign_elem(
    src: (&SourceSet, &PushVec<Node>),
    scope: Index,
    kind: BlockKind,
    dst: Index,
    assign: Assign,
    dir: &mut Dir,
) -> Result<(), Error> {
    let node = &src.1[dst];
    if let Some(ident) = node.get_ident() {
        let elem = match kind {
            BlockKind::Normal => AssignElem::Assign(assign),
            BlockKind::Selector(_) => unreachable!(),
            BlockKind::SelectorBlock(selector) => {
                AssignElem::Selector { selector, assign }
            }
        };
        return push_assign_elem(scope, ident, elem, dir);
    }
    if node.is_output() {
        let outer = dir.scopes[scope].outer().expect("no outer scope");
        let elem = match kind {
            BlockKind::Normal => AssignElem::Assign(assign),
            BlockKind::Selector(_) => unreachable!(),
            BlockKind::SelectorBlock(selector) => {
                AssignElem::Selector { selector, assign }
            }
        };
        return push_assign_elem(outer, "", elem, dir);
    }
    if let Some((lhs, rhs)) = node.get_expr_field() {
        if src.1[lhs].is_output() {
            let outer = dir.scopes[scope].outer().expect("no outer scope");
            return add_assign_elem(src, outer, kind, rhs, assign, dir);
        }
    }
    unimplemented!()
}

fn push_assign_elem(
    scope: Index,
    ident: &str,
    elem: AssignElem,
    dir: &mut Dir,
) -> Result<(), Error> {
    if let Some(i) = dir.scopes[scope].get(ident) {
        dir.assigns[i].push(elem);
    } else {
        let i = dir.assigns.push(AssignGroup::One(elem));
        dir.scopes[scope].insert(ident.into(), i);
    }
    Ok(())
}

pub fn resolve(
    sources: &SourceSet,
    parse_nodes: &PushVec<Node>,
    parse_root: Index,
    nodes: &mut PushVec<Node>,
) -> Result<Index, Error> {
    let dir = create_dir(sources, parse_nodes, parse_root)?;
    let _ = dir;
    let root = node::root(Box::new([]), nodes);
    Ok(root)
}
