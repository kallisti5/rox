// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

extern crate rug;

macro_rules! static_assert {
    ($cond:expr) => {
        let _: [(); (!$cond) as usize] = [];
    };
}

macro_rules! static_assert_size {
    ($T:ty, $U:ty) => {
        let _ = |t: $T| unsafe { ::std::mem::transmute::<_, $U>(t) };
    };
    ($T:ty : $size:expr) => {
        static_assert_size!($T, [u8; $size as usize]);
    };
}

#[macro_use]
mod try_next;

mod utils;

pub mod error;
pub mod eval;
pub mod input;
pub mod map;
pub mod netlist;
pub mod node;
pub mod output;
pub mod package;
pub mod parse;
pub mod push_vec;
pub mod resolve;
