// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

pub trait OrTryNext {
    fn or_try_next<F: FnOnce() -> Self>(self, f: F) -> Self
    where
        Self: Sized;
}

impl<T, E> OrTryNext for Result<Option<T>, E> {
    fn or_try_next<F: FnOnce() -> Self>(self, f: F) -> Self {
        match self {
            Ok(None) => f(),
            _ => self,
        }
    }
}

// macro_rules! unwrap_or_try_next {
//     ($e:expr) => {
//         match $e {
//             Some(s) => s,
//             None => return Ok(None),
//         }
//     }
// }
