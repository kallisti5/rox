// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use error::Error;
use input::SourceSet;
use node::Node;
use push_vec::{Index, PushVec};

pub fn output(
    sources: &SourceSet,
    nodes: &PushVec<Node>,
    root: Index,
) -> Result<(), Error> {
    let _ = (sources, nodes, root);
    Ok(())
}
