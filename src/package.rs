// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

use error::Error;
use eval;
use input::SourceSet;
use node::{self, Node};
use output;
use parse;
use push_vec::{OptionIndex, PushVec};
use resolve;

use std::fmt::{Debug, Display, Formatter, Result as FmtResult};
use std::io::{Result as IoResult, Write};

pub struct Package {
    sources: SourceSet,
    parse_nodes: PushVec<Node>,
    parse_root: OptionIndex,
    resolve_nodes: PushVec<Node>,
    resolve_root: OptionIndex,
    evaluate_nodes: PushVec<Node>,
    evaluate_root: OptionIndex,
}

impl Default for Package {
    fn default() -> Package {
        Package::new()
    }
}

impl Package {
    pub fn new() -> Package {
        Package {
            sources: SourceSet::new(),
            parse_nodes: PushVec::new(),
            parse_root: None.into(),
            resolve_nodes: PushVec::new(),
            resolve_root: None.into(),
            evaluate_nodes: PushVec::new(),
            evaluate_root: None.into(),
        }
    }

    fn dump(
        &self,
        write: &mut Write,
        verbose: bool,
        nodes: &PushVec<Node>,
        root: OptionIndex,
    ) -> IoResult<()> {
        let root = match root.as_option() {
            Some(s) => s,
            None => return Ok(()),
        };
        write!(write, "Sources:")?;
        for (_, filename) in self.sources.sources() {
            write!(write, " {}", self.sources.get_filename(filename))?;
        }
        write!(write, "\n")?;
        let sources = if verbose { Some(&self.sources) } else { None };
        write!(write, "{}", &node::display(sources, nodes, root))?;
        Ok(())
    }

    pub fn parse_sources(&mut self, filenames: &[&str]) -> Result<(), Error> {
        let mut packages = Vec::new();
        let mut files = Vec::new();
        self.sources.read(filenames)?;
        for (mut span, _) in self.sources.sources() {
            let file =
                parse::parse(&self.sources, &mut span, &mut self.parse_nodes)?;
            files.push(file);
        }
        let package =
            node::package(files.into_boxed_slice(), &mut self.parse_nodes);
        packages.push(package);
        self.parse_root =
            node::root(packages.into_boxed_slice(), &mut self.parse_nodes)
                .into();
        Ok(())
    }

    pub fn dump_parse(&self, write: &mut Write, verbose: bool) -> IoResult<()> {
        self.dump(write, verbose, &self.parse_nodes, self.parse_root)
    }

    pub fn resolve_idents(&mut self) -> Result<(), Error> {
        let parse_root = match self.parse_root.as_option() {
            Some(s) => s,
            None => return Ok(()),
        };
        let index = resolve::resolve(
            &self.sources,
            &self.parse_nodes,
            parse_root,
            &mut self.resolve_nodes,
        )?;
        self.resolve_root = index.into();
        Ok(())
    }

    pub fn dump_resolve(
        &self,
        write: &mut Write,
        verbose: bool,
    ) -> IoResult<()> {
        self.dump(write, verbose, &self.resolve_nodes, self.resolve_root)
    }

    pub fn evaluate(&mut self) -> Result<(), Error> {
        let resolve_root = match self.resolve_root.as_option() {
            Some(s) => s,
            None => return Ok(()),
        };
        let index = eval::evaluate(
            &self.sources,
            &self.resolve_nodes,
            resolve_root,
            &mut self.evaluate_nodes,
        )?;
        self.evaluate_root = index.into();
        Ok(())
    }

    pub fn dump_eval(&self, write: &mut Write, verbose: bool) -> IoResult<()> {
        self.dump(write, verbose, &self.evaluate_nodes, self.evaluate_root)
    }

    pub fn dump_error(
        &self,
        _write: &mut Write,
        _verbose: bool,
    ) -> IoResult<()> {
        Ok(())
    }

    pub fn output(&self) -> Result<(), Error> {
        let evaluate_root = match self.evaluate_root.as_option() {
            Some(s) => s,
            None => return Ok(()),
        };
        output::output(&self.sources, &self.evaluate_nodes, evaluate_root)
    }
}

impl Debug for Package {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        fmt.write_str("Sources:")?;
        for (_, filename) in self.sources.sources() {
            fmt.write_str(" ")?;
            Display::fmt(self.sources.get_filename(filename), fmt)?;
        }

        if let Some(parse_root) = self.parse_root.as_option() {
            fmt.write_str("\nParse tree:\n")?;
            Display::fmt(
                &node::display(
                    Some(&self.sources),
                    &self.parse_nodes,
                    parse_root,
                ),
                fmt,
            )?;
        }

        if let Some(resolve_root) = self.resolve_root.as_option() {
            fmt.write_str("\nResolve tree:\n")?;
            Display::fmt(
                &node::display(
                    Some(&self.sources),
                    &self.parse_nodes,
                    resolve_root,
                ),
                fmt,
            )?;
        }

        Ok(())
    }
}
