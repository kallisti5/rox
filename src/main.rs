// Copyright © 2016–2018 University of Malta

// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#[macro_use]
extern crate clap;
extern crate rox;

use clap::Arg;
use rox::error::Error as RoxError;
use rox::package::Package;
use std::error::Error;
use std::fs::File;
use std::io::{self, BufWriter, Error as IoError, Result as IoResult, Write};
use std::process;

#[derive(Clone, Copy, Debug, Hash, Eq, Ord, PartialEq, PartialOrd)]
struct Options<'a> {
    filenames: &'a [&'a str],
    stop_after: Option<&'a str>,
    verbose_dump: bool,
    dump_parse: Option<&'a str>,
    dump_resolve: Option<&'a str>,
    dump_eval: Option<&'a str>,
    dump_error: Option<&'a str>,
}

fn err_out(filename: &str, err: IoError) -> RoxError {
    let filename = if filename == "" {
        "<standard output>"
    } else {
        filename
    };
    RoxError::io(filename, err)
}

fn get_writer(filename: &str) -> Result<Box<Write>, RoxError> {
    if filename == "" {
        Ok(Box::new(std::io::stdout()))
    } else {
        match File::create(filename).map(BufWriter::new) {
            Ok(writer) => Ok(Box::new(writer)),
            Err(e) => Err(err_out(filename, e)),
        }
    }
}

trait TranslateStep {
    fn work<Work>(&mut self, work: Work) -> &mut Self
    where
        Self: Sized,
        Work: FnMut() -> Self;

    fn dump<Dump>(&mut self, file: Option<&str>, dump: Dump) -> &mut Self
    where
        Dump: FnMut(&mut Write) -> IoResult<()>;
}

impl TranslateStep for Result<(), RoxError> {
    fn work<Work>(&mut self, mut work: Work) -> &mut Result<(), RoxError>
    where
        Work: FnMut() -> Result<(), RoxError>,
    {
        if self.is_ok() {
            *self = work();
        }
        self
    }

    fn dump<Dump>(
        &mut self,
        file: Option<&str>,
        mut dump: Dump,
    ) -> &mut Result<(), RoxError>
    where
        Dump: FnMut(&mut Write) -> IoResult<()>,
    {
        if self.is_ok() {
            if let Some(filename) = file {
                *self = get_writer(filename).and_then(|mut w| {
                    dump(&mut w).map_err(|e| err_out(filename, e))
                });
            }
        }
        self
    }
}

fn translate(options: Options) -> Result<(), RoxError> {
    let mut package = Package::new();
    let mut ret = Ok(());
    let mut stop = false;
    let v = options.verbose_dump;
    if !stop {
        ret.work(|| package.parse_sources(options.filenames))
            .dump(options.dump_parse, |w| package.dump_parse(w, v));
        stop = options.stop_after == Some("parse");
    }
    if !stop {
        ret.work(|| package.resolve_idents())
            .dump(options.dump_resolve, |w| package.dump_parse(w, v));
        stop = stop || options.stop_after == Some("resolve");
    }
    if !stop {
        ret.work(|| package.evaluate())
            .dump(options.dump_eval, |w| package.dump_eval(w, v));
        stop = stop || options.stop_after == Some("evaluate");
    }
    if !stop {
        ret.work(|| package.output());
    }
    if ret.is_err() {
        Ok(()).dump(options.dump_error, |w| package.dump_error(w, v));
    }
    ret
}

fn main() {
    let app = app_from_crate!("\n")
        .arg(
            Arg::with_name("INPUT")
                .help("The input files to translate")
                .required(true)
                .multiple(true)
                .index(1),
        )
        .arg(
            Arg::with_name("stop-after")
                .long("stop-after")
                .takes_value(true)
                .value_name("PASS")
                .possible_values(&["parse", "resolve", "evaluate"])
                .help("Stops after executing the specified pass"),
        )
        .arg(
            Arg::with_name("verbose-dump")
                .long("verbose-dump")
                .help("More verbose tree dumps"),
        )
        .arg(
            Arg::with_name("dump-parse")
                .long("dump-parse")
                .takes_value(true)
                .value_name("FILENAME")
                .help("Dumps the tree after parsing"),
        )
        .arg(
            Arg::with_name("dump-resolve")
                .long("dump-resolve")
                .takes_value(true)
                .value_name("FILENAME")
                .help("Dumps the tree after resolving identifiers"),
        )
        .arg(
            Arg::with_name("dump-evaluate")
                .long("dump-evaluate")
                .takes_value(true)
                .value_name("FILENAME")
                .help("Dumps the tree after evaluating expressions"),
        )
        .arg(
            Arg::with_name("dump-error")
                .long("dump-error")
                .takes_value(true)
                .value_name("FILENAME")
                .help("Dumps the tree if an error occurs"),
        );
    let matches = app.get_matches();
    let options = Options {
        filenames: &matches
            .values_of("INPUT")
            .expect("required INPUT")
            .collect::<Vec<_>>(),
        stop_after: matches.value_of("stop-after"),
        verbose_dump: matches.is_present("verbose-dump"),
        dump_parse: matches.value_of("dump-parse"),
        dump_resolve: matches.value_of("dump-resolve"),
        dump_eval: matches.value_of("dump-evaluate"),
        dump_error: matches.value_of("dump-error"),
    };
    match translate(options) {
        Ok(_) => {}
        Err(e) => {
            writeln!(&mut io::stderr(), "{}", e.description()).unwrap();
            process::exit(2);
        }
    }
}
